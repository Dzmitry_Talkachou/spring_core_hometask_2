<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h2>Event</h2>

<input type="button" value="all-event"
	onclick="return location.href = '${pageContext.request.contextPath}/event/all'" />

<table class="events">
	<c:forEach var="event" items="${eventAll}">
		<tr>
			<td>${event.eventId}</td>
			<td>${event.eventName}</td>
			<td>${event.rating}</td>
			<td>${event.basePrice}</td>
			<td><input type="button" value="edit"
				onclick="return location.href = '${pageContext.request.contextPath}/event/edit/${event.eventId}'" /></td>
			<td><input type="button" value="delete"
				onclick="return location.href = '${pageContext.request.contextPath}/event/delete/${event.eventId}'" /></td>
		</tr>
	</c:forEach>
</table>