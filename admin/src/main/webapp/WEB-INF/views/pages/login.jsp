<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h2>Login</h2>
<form name="login-form"
	action="<c:url value='j_spring_security_check' />" method="post">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<table>
		<tr>
			<td>login</td>
			<td><input type='text' name='username' placeholder="login" /></td>
		</tr>
		<tr>
			<td>password</td>
			<td><input type='password' name='password'
				placeholder="password" /></td>
		</tr>
		<tr>
			<td><input type="submit" name="button" value="enter" /></td>
		</tr>
	</table>
</form>