package com.epam.theatre.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.theatre.service.EventService;

@Controller
@RequestMapping("/")
public class EventController {

	@Autowired
	EventService eventService;

	@RequestMapping(value = { "/event" }, method = RequestMethod.GET)
	public String eventPage(ModelMap model) {
		return "event";
	}

	@RequestMapping(value = { "/event/all" }, method = RequestMethod.GET)
	public ModelAndView eventAllPage(ModelAndView model) {
		model.addObject("eventAll", eventService.getAll()).setViewName("event");
		return model;
	}

	@RequestMapping(value = { "/event/delete/{Id}" }, method = RequestMethod.GET)
	public ModelAndView eventDeletePage(ModelAndView model) {
		model.setViewName("event-delete");
		return model;
	}

	@RequestMapping(value = { "/event/edit/{eventId}" }, method = RequestMethod.GET)
	public ModelAndView eventEditPage(@PathVariable(value = "eventId") Long eventId,
			ModelAndView model) {

		model.addObject("event", eventService.getById(eventId)).setViewName("event");
		model.setViewName("event-edit");
		return model;
	}
	
	@RequestMapping(value = { "/event/save" }, method = RequestMethod.GET)
	public ModelAndView eventSave(ModelAndView model) {
		model.addObject("eventAll", eventService.getAll()).setViewName("event");
		return model;
	}

}