package com.epam.theatre.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.epam.theatre.service.AuthenticationService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin").password("admin").roles("2");

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/resources/**")//
				.permitAll();
		http.formLogin().loginPage("/login")//
				.loginProcessingUrl("/j_spring_security_check")//
				.failureUrl("/login?error")//
				.usernameParameter("username")//
				.passwordParameter("password")//
				.permitAll();
		http.authorizeRequests().antMatchers("/contactus**")//
				.access("hasRole(2)");
		http.logout()//
				.permitAll()//
				.logoutUrl("/logout")//
				.logoutSuccessUrl("/login?logout")//
				.invalidateHttpSession(true);
	}

}