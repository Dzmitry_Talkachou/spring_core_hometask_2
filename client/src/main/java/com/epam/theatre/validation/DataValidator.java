package com.epam.theatre.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Enum DataValidator.
 */
public enum DataValidator {

	/** The number. */
	NUMBER("^0$|^[1-9]\\d{0,7}$"),

	/** The id. */
	ID("^[1-9]\\d{0,7}$"),

	/** The first name. */
	FIRST_NAME(
			"^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}$|^[A-Z][a-z']{1,25}(-|\\s)[A-Z][a-z'.]{2,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}(-|\\s)[\u0410-\u042F][\u0430-\u044F'.]{2,25}$"),

	/** The second name. */
	SECOND_NAME(
			"^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}$|^[A-Z][a-z']{1,25}(-|\\s)[A-Z][a-z'.]{2,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}(-|\\s)[\u0410-\u042F][\u0430-\u044F'.]{2,25}$"),

	/** The city name. */
	CITY_NAME(
			"^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}$|^[A-Z][a-z']{1,25}[-|\\s][A-Z][a-z']{2,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}-[\u0410-\u042F][\u0430-\u044F']{2,25}$"),

	/** The country name. */
	COUNTRY_NAME(
			"^[A-Z]{2,5}$|^[\u0410-\u042F]$|^[A-Z]([a-z']){1,60}$|^[\u0410-\u042F]([\u0430-\u044F']){1,60}$|^([A-Z]([a-z']){1,25}(-[A-Z][a-z']|\\s[A-Z][a-z']){0,1}([a-z']){0,20})(-[A-Z][a-z']|\\s[A-Z][a-z'])([a-z']){0,20}$|^([\u0410-\u042F]([\u0430-\u044F']){1,25}(-[\u0410-\u042F]|\\s[\u0410-\u042F]){0,1}([\u0430-\u044F']){0,20})(-[\u0410-\u042F]|\\s[\u0410-\u042F])([\u0430-\u044F']){0,20}$"),

	/** The model name. */
	MODEL_NAME(
			"^[A-Z][a-z'-']{1,20}[\\s][A-Za-z'0-9-]{1,15}[A-Za-z'0-9]{1,15}$|^[\u0410-\u042F][\u0430-\u044F'-']{1,20}[\\s][\u0410-\u042F\u0430-\u044F'0-9-|A-Za-z'0-9-]{1,15}[\u0410-\u042F\u0430-\u044F'0-9|A-Za-z'0-9]{1,15}$"),

	/** The email. */
	EMAIL("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$"),

	/** The date. */
	DATE("^([1-9]|[1-2][0-9]|3[0-1])-([1][0-2]|[1-9])-(201[6-9]|203[0-7])\\s(([2][0-3]|[1][0-9]|[1-9]):([0-5][0-9]))$"),

	/** The password. */
	PASSWORD("^(?=.*\\d)(?=.*[a-z'])(?=.*[A-Z])(?!.*\\s).*$"),

	/** The tail number. */
	TAIL_NUMBER("^SU-[1-2]\\d{3}$"),

	/** The fuel consumption. */
	FUEL_CONSUMPTION("^[5-9]\\d{2}$|^[1-4]\\d{3}$"),

	/** The passenger capacity. */
	PASSENGER_CAPACITY("^[2-9]\\d{1}$|^[1-9]\\d{2}$"),

	/** The route name. */
	ROUTE_NAME("^[A-Z][A-Z]-[1-9]\\d{3}$"),

	/** The speed. */
	SPEED("^[2-9]\\d{2}$|^[1]\\d{3}$"),

	/** The distance. */
	DISTANCE("^[1-9]\\d{2}$|^[1-9]\\d{3}$|^[1]\\d{4}$"),

	/** The locale. */
	LOCALE("^[a-z]{2}_[A-Z]{2}$"),

	/** The float. */
	FLOAT("^([+-]?\\d*\\.?\\d*)$");

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(DataValidator.class);

	/** The pattern. */
	private Pattern pattern;
	
	/**
	 * Instantiates a new data validator.
	 *
	 * @param regEx the regular expression
	 */
	DataValidator(String regEx) {
		this.pattern = Pattern.compile(regEx);
	}

	/**
	 * Check.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public boolean check(String data) {
		boolean value = false;
		if (data != null) {
			Matcher matcher = this.pattern.matcher(data);
			if (matcher.matches()) {
				value = true;
			} else {
				LOGGER.log(Level.INFO, "Validaton failed: " + this + " - '" + data + "'");
			}
		} else {
			LOGGER.log(Level.INFO, "Validaton failed: " + this + " - null");
		}
		return value;
	}
}