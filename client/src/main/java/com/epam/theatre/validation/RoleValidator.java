package com.epam.theatre.validation;

/**
 * The Enum RoleValidator.
 */
public enum RoleValidator {

	/** The user. */
	USER("1"),
	/** The admin. */
	ADMIN("5");

	/** The role id. */
	private String roleId;

	/**
	 * Instantiates a new role validator.
	 *
	 * @param role
	 *            the role
	 */
	RoleValidator(String role) {
		this.roleId = role;
	}

	/**
	 * Check.
	 *
	 * @param checkedRole
	 *            the checked role
	 * @return true, if successful
	 */
	public boolean check(String checkedRole) {
		boolean value = false;
		if (checkedRole != null) {
			if (this.roleId.equals(checkedRole)) {
				value = true;
			}
		}
		return value;
	}
}