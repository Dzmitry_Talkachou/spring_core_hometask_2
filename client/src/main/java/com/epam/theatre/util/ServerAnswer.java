package com.epam.theatre.util;

/**
 * The Class ServerAnswer.
 */
public class ServerAnswer {

	/** The message. */
	private ServerMessage message = new ServerMessage();

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public ServerMessage getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message.setMessage(message);
	}

	/**
	 * Sets the message.
	 *
	 * @param message the message
	 * @param result the result
	 */
	public void setMessage(String message, boolean result) {
		this.message.setMessage(message, result);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerAnswer other = (ServerAnswer) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ServerAnswer [message=" + message + "]";
	}
}