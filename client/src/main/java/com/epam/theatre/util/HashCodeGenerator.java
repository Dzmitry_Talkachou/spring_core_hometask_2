package com.epam.theatre.util;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class HashCodeGenerator {

	public String createHashedValue(String word) {

		Md5PasswordEncoder encode = new Md5PasswordEncoder();

		return encode.encodePassword(word, null);
	}
}