package com.epam.theatre.util;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * The Class JsonConvertor.
 *
 * @param <T>
 *            the generic type
 */
public class JsonConvertor<T> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(JsonConvertor.class);

	/**
	 * To json.
	 *
	 * @param t
	 *            the t
	 * @return the string
	 */
	public String toJson(List<T> t) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		String json = null;

		if (!t.isEmpty()) {
			try {
				json = mapper.writeValueAsString(t);
			}
			catch (JsonProcessingException e) {
				LOGGER.log(Level.ERROR, "JsonProcessingException in method String toJson(List<T> t)" + e);
			}
		}
		return json;
	}

	/**
	 * To json.
	 *
	 * @param t
	 *            the t
	 * @return the string
	 */
	public String toJson(T t) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		String json = null;
		if (t != null) {
			try {
				json = mapper.writeValueAsString(t);
			}
			catch (JsonProcessingException e) {
				LOGGER.log(Level.ERROR, "JsonProcessingException in method String toJson(T t)" + e);
			}
		}
		return json;
	}
}