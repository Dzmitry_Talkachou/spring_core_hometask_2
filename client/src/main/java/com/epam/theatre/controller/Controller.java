package com.epam.theatre.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.epam.theatre.action.Action;
import com.epam.theatre.config.AppConfigClient;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.manager.MessageManager;
import com.epam.theatre.util.JsonConvertor;
import com.epam.theatre.util.ServerAnswer;
import com.epam.theatre.util.ServerMessage;

@WebServlet(name = "controller", urlPatterns = "/controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = -4582378231457018142L;

	private static final String LOCALE = "locale";
	private static final String COMMAND = "command";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(Controller.class);

	private AbstractApplicationContext context;

	@Override
	public void init() throws ServletException {
		super.init();
		context = new AnnotationConfigApplicationContext(AppConfigClient.class);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String command = request.getParameter(COMMAND);
		LOGGER.log(Level.INFO, command);

		String locale = (String) request.getSession().getAttribute(LOCALE);
		com.epam.theatre.util.JsonConvertor<ServerMessage> jsonConv = new JsonConvertor<>();
		ServerAnswer serverAnswer = new ServerAnswer();
		String json = null;

		try {
			Action action = (Action) context.getBean(command);

			json = action.execute(request);
			LOGGER.log(Level.INFO, json);

		} catch (CommandException | NoSuchBeanDefinitionException e) {
			LOGGER.log(Level.ERROR, "CommandException in Controller class", e);
			serverAnswer.setMessage(MessageManager.getProperty(MessageEnum.SERVER_ERROR, locale));
			json = jsonConv.toJson(serverAnswer.getMessage());
		}

		if (json == null) {
			LOGGER.log(Level.ERROR, "json is null");
			serverAnswer.setMessage(MessageManager.getProperty(MessageEnum.SERVER_ERROR, locale));
			json = jsonConv.toJson(serverAnswer.getMessage());
		}
		response.getWriter().write(json);

	}
}