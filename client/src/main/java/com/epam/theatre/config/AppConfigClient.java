package com.epam.theatre.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.epam.theatre")
@Import(AppConfigCommon.class)
public class AppConfigClient {

}
