package com.epam.theatre.page;

public class ResponsePage {

	private String page;
	private boolean isRedirect;

	public ResponsePage(String page, boolean isRedirect) {
		this.page = page;
		this.isRedirect = isRedirect;
	}

	public boolean isRedirect() {
		return isRedirect;
	}

	@Override
	public String toString() {
		return page;
	}
}
