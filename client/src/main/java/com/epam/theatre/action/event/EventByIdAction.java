package com.epam.theatre.action.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Event;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.EventService;
import com.epam.theatre.util.JsonConvertor;
import com.epam.theatre.validation.DataValidator;

@Component("event-by-id")
public class EventByIdAction extends AbstractAction {


	private static final String EVENT_ID = "event-id";

	@Autowired
	EventService eventService;

	protected void doAction() throws CommandException {
		Event event = null;

		try {
			event = eventService.getById(Long.valueOf(getRequestParameter(EVENT_ID)));
		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in EventByIdCommand class", e);
		}
		JsonConvertor<Event> jsonConv = new JsonConvertor<>();
		setJsonResult(jsonConv.toJson(event));
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.ID.check(getRequestParameter(EVENT_ID));
	}

	@Override
	protected boolean roleValidate() {
		return true;
	}
}