package com.epam.theatre.action;

import javax.servlet.http.HttpServletRequest;

import com.epam.theatre.exception.CommandException;

public interface Action {

	String execute(HttpServletRequest request) throws CommandException;
}