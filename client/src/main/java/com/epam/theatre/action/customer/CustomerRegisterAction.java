package com.epam.theatre.action.customer;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.util.HashCodeGenerator;
import com.epam.theatre.validation.DataValidator;

@Component("customer-create")
public class CustomerRegisterAction extends AbstractAction {

	/** The Constant FIRST_NAME. */
	private static final String FIRST_NAME = "first-name";

	/** The Constant SECOND_NAME. */
	private static final String SECOND_NAME = "second-name";

	/** The Constant PASSWORD. */
	private static final String PASSWORD = "password";

	// /** The Constant PAYMENT_METHOD_ID. */
	// private static final String PAYMENT_METHOD_ID = "payment-method-id";

	/** The Constant EMAIL. */
	private static final String EMAIL = "email";

	/** The Constant ROLE_ID. */
	private static final Long ROLE_ID = 1L; // default role - customer

	/** The Constant CUSTOMER_ROLE_ID. */
	private static final String CUSTOMER_ROLE_ID = "roleId";

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customerId";

	/** The Constant CUSTOMER_NAME. */
	private static final String CUSTOMER_NAME = "customerName";

	@Autowired
	CustomerService customerService;

	@Override
	protected void doAction() throws CommandException {
		HashCodeGenerator hashCode = new HashCodeGenerator();
		Customer customer = new Customer();

		try {
			customer.setFirstName(getRequestParameter(FIRST_NAME));
			customer.setLastName(getRequestParameter(SECOND_NAME));
			customer.setPassword(hashCode.createHashedValue(getRequestParameter(PASSWORD)));
			// customer.setPaymentMethodId(Long.valueOf(getRequestParameter(PAYMENT_METHOD_ID)));
			 customer.setRoleId(ROLE_ID);
			// customer.setTicketsPurchase(TICKETS_PURCHASE);
			customer.setBirthDay(LocalDate.now());
			customer.setEmail(getRequestParameter(EMAIL));

			Customer oldCustmer = customerService.takeByEmail(getRequestParameter(EMAIL));
			if (oldCustmer == null) {
				Long customerId = customerService.save(customer);
				customer = customerService.getById(customerId);

				setSessionAttribute(CUSTOMER_ROLE_ID, String.valueOf(ROLE_ID));
				setSessionAttribute(CUSTOMER_ID, (String.valueOf(customerId)));
				setSessionAttribute(CUSTOMER_NAME, getRequestParameter(FIRST_NAME));

				successfulCreateMessage();

			} else {
				messageToJson(MessageEnum.ERROR_WRONG_EMAIL, false);

			}

		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in CustomerCreateCommand class", e);
		}
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.FIRST_NAME.check(getRequestParameter(FIRST_NAME))
				&& DataValidator.SECOND_NAME.check(getRequestParameter(SECOND_NAME))
				&& DataValidator.PASSWORD.check(getRequestParameter(PASSWORD))
				&& DataValidator.EMAIL.check(getRequestParameter(EMAIL));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.nyair.command.AbstractCommand#roleValidate()
	 */
	@Override
	protected boolean roleValidate() {
		// all users
		return true;
	}
}