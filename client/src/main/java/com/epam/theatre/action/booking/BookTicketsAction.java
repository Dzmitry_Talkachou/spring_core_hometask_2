package com.epam.theatre.action.booking;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Ticket;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.service.BookingService;
import com.epam.theatre.util.JsonConvertor;

@Component("book-tickets")
public class BookTicketsAction extends AbstractAction {

	@Autowired
	BookingService bookingService;

	private static final String SEATS = "seats";
	private static final String SCHEDULE_ID = "schedule-id";

	@Override
	protected void doAction() throws CommandException {

		Set<Long> seats = Pattern.compile(",").splitAsStream(getRequestParameter(SEATS)).map(Long::parseLong)
				.collect(Collectors.toSet());
		Long scheduleId = Long.valueOf(getRequestParameter(SCHEDULE_ID));

		Long customerId = null;

		if (takeCustomerId() != null) {
			customerId = Long.valueOf(takeCustomerId());
		}

		Set<Long> checkedSeats = bookingService.checkSeats(seats, scheduleId);
		Set<Ticket> tickets = bookingService.takeTicketsWithPrices(scheduleId, checkedSeats, customerId);
		
		Set<Long> ticketsId = bookingService.bookTickets(tickets);
		System.out.println(ticketsId);
		if (ticketsId.size() != 0) {

			JsonConvertor<Set<Ticket>> jsonConv = new JsonConvertor<>();
			setJsonResult(jsonConv.toJson(tickets));

			messageToJson(MessageEnum.THANK_YOU_FOR_PURCHASING, false);
		} else {
			messageToJson(MessageEnum.TICKETS_FOR_THIS_FLIGHT_IS_NO_MORE, false);
		}
	}

	@Override
	protected boolean dataValidate() {
		return true;
	}

	@Override
	protected boolean roleValidate() {
		// all roles
		return true;
	}

}
