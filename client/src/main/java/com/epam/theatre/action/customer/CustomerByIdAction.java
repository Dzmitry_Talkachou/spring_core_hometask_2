package com.epam.theatre.action.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.util.JsonConvertor;
import com.epam.theatre.validation.DataValidator;
import com.epam.theatre.validation.RoleValidator;

@Component("customer-by-id")
public class CustomerByIdAction extends AbstractAction {

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customer-id";

	@Autowired
	CustomerService customerService;

	@Override
	protected void doAction() throws CommandException {
		Customer customer = null;

		try {
			customer = customerService.getById(Long.valueOf(getRequestParameter(CUSTOMER_ID)));
			customer.setPassword(null);
		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in CustomerByIdCommand class", e);
		}
		JsonConvertor<Customer> jsonConv = new JsonConvertor<>();
		setJsonResult(jsonConv.toJson(customer));
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.ID.check(getRequestParameter(CUSTOMER_ID));
	}

	@Override
	protected boolean roleValidate() {
		return takeCustomerId().equals(getRequestParameter(CUSTOMER_ID)) || RoleValidator.ADMIN.check(takeRoleId());
	}
}