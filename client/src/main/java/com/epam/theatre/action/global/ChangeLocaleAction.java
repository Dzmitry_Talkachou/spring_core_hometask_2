package com.epam.theatre.action.global;

import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.constant.LocaleConstant;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.validation.DataValidator;

@Component("change-locale")
public class ChangeLocaleAction extends AbstractAction {

	private static final String LOCALE = "locale";

	@Override
	protected void doAction() throws CommandException {

		switch (getRequestParameter(LOCALE)) {

		case LocaleConstant.EN_GB:
			setSessionAttribute(LOCALE, LocaleConstant.EN_GB);
			messageToJson(MessageEnum.INFO_ENGLISH_LOCALE, true);
			break;

		case LocaleConstant.RU_BY:
			setSessionAttribute(LOCALE, LocaleConstant.RU_BY);
			messageToJson(MessageEnum.INFO_RUSSIAN_LOCALE, true);
			break;

		default:
			messageToJson(MessageEnum.INFO_ERROR_LOCALE, false);
		}
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.LOCALE.check(getRequestParameter(LOCALE));
	}

	@Override
	protected boolean roleValidate() {
		// all roles
		return true;
	}
}