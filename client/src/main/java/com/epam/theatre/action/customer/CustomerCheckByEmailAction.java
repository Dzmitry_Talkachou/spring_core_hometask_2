package com.epam.theatre.action.customer;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.validation.DataValidator;

public class CustomerCheckByEmailAction extends AbstractAction {

	private static final String EMAIL = "email";

	@Autowired
	CustomerService customerService;

	@Override
	protected void doAction() throws CommandException {
		Customer customer = null;
		try {

			customer = customerService.takeByEmail(getRequestParameter(EMAIL));
			if (customer != null) {
				errorDataMessage();
			} else {
				sussefulFindMessage();
			}

		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in CustomerCreateCommand class", e);
		}
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.EMAIL.check(getRequestParameter(EMAIL));
	}

	@Override
	protected boolean roleValidate() {
		// all roles
		return true;
	}
}