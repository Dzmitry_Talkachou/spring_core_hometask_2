package com.epam.theatre.action.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.EventSchedule;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.EventScheduleService;
import com.epam.theatre.util.JsonConvertor;
import com.epam.theatre.validation.DataValidator;

@Component("schedule-by-id")
public class EventScheduleById extends AbstractAction {

	/** The Constant CUSTOMER_ID. */
	private static final String SCHEDULE_ID = "schedule-id";

	@Autowired
	EventScheduleService eventScheduleService;

	@Override
	protected void doAction() throws CommandException {
		EventSchedule eventSchedule;

		eventSchedule = eventScheduleService.getById(Long.valueOf(getRequestParameter(SCHEDULE_ID)));

		JsonConvertor<EventSchedule> jsonConv = new JsonConvertor<>();
		setJsonResult(jsonConv.toJson(eventSchedule));
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.ID.check(getRequestParameter(SCHEDULE_ID));
	}

	@Override
	protected boolean roleValidate() {
		return true;
	}
}