package com.epam.theatre.action.global;

import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.util.JsonConvertor;

@Component("login-customer-info")
public class CustomerInfoAction extends AbstractAction {

	/** The Constant CUSTOMER_ROLE_ID. */
	private static final String CUSTOMER_ROLE_ID = "roleId";

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customerId";

	/** The Constant CUSTOMER_NAME. */
	private static final String CUSTOMER_NAME = "customerName";

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.nyair.command.AbstractCommand#doAction()
	 */
	@Override
	protected void doAction() throws CommandException {
		Customer customer = new Customer();
		try {
			customer.setCustomerId(Long.valueOf(getSessionAttribute(CUSTOMER_ID)));
			customer.setRoleId(Long.valueOf(getSessionAttribute(CUSTOMER_ROLE_ID)));
			customer.setFirstName((getSessionAttribute(CUSTOMER_NAME)));

		} catch (NumberFormatException e) {
			throw new CommandException("Exception in LoginInfoCommand class", e);
		}
		JsonConvertor<Customer> jsonConv = new JsonConvertor<>();
		setJsonResult(jsonConv.toJson(customer));
	}


	@Override
	protected void errorDataMessage() {
		messageToJson(MessageEnum.INFO_ALREADY_LOGOUT, false);
	}

	@Override
	protected boolean dataValidate() {
		boolean value = false;
		if (getSessionAttribute(CUSTOMER_ID) != null && getSessionAttribute(CUSTOMER_ROLE_ID) != null
				&& getSessionAttribute(CUSTOMER_NAME) != null) {
			value = true;
		}
		return value;
	}

	@Override
	protected boolean roleValidate() {
		// All roles
		return true;
	}
}