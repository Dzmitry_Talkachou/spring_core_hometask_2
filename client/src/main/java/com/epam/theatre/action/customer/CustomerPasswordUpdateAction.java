package com.epam.theatre.action.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.util.HashCodeGenerator;
import com.epam.theatre.validation.DataValidator;

@Component("customer-password-update")
public class CustomerPasswordUpdateAction extends AbstractAction {

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customer-id";

	/** The Constant NEW_PASSWORD. */
	private static final String NEW_PASSWORD = "password";

	/** The Constant OLD_PASSWORD. */
	private static final String OLD_PASSWORD = "old-password";

	@Autowired
	CustomerService customerService;

	@Override
	protected void doAction() throws CommandException {
		HashCodeGenerator hashCode = new HashCodeGenerator();
		Customer updatedCustomer = new Customer();

		try {
			Long customerId = Long.valueOf(getRequestParameter(CUSTOMER_ID));
			String newPassword = getRequestParameter(NEW_PASSWORD);

			if (newPassword != null) {
				customerService.updateByPasswordAndId(customerId, getRequestParameter(OLD_PASSWORD), newPassword);
				customerService.getById(customerId);

				if (hashCode.createHashedValue(newPassword).equals(updatedCustomer.getPassword())) {
					successfulUpdateMessage();
				} else {
					unsuccessfulUpdateMessage();
				}
			} else {
				successfulUpdateMessage();
			}

		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in CustomerUpdateCommand class", e);
		}
	}

	@Override
	protected boolean dataValidate() {
		String newPassword = getRequestParameter(NEW_PASSWORD);
		boolean value = DataValidator.ID.check(getRequestParameter(CUSTOMER_ID));

		if (value && newPassword != null) {
			value = DataValidator.PASSWORD.check(newPassword)
					&& DataValidator.PASSWORD.check(getRequestParameter(OLD_PASSWORD));
		}
		return value;
	}

	@Override
	protected boolean roleValidate() {
		return takeCustomerId().equals(getRequestParameter(CUSTOMER_ID));
	}
}