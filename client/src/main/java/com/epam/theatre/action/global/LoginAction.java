package com.epam.theatre.action.global;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.util.HashCodeGenerator;
import com.epam.theatre.validation.DataValidator;

@Component("login")
public class LoginAction extends AbstractAction {

	/** The Constant EMAIL. */
	private static final String EMAIL = "email";

	/** The Constant PASSWORD. */
	private static final String PASSWORD = "password";

	/** The Constant CUSTOMER_ROLE_ID. */
	private static final String CUSTOMER_ROLE_ID = "roleId";

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customerId";

	/** The Constant CUSTOMER_NAME. */
	private static final String CUSTOMER_NAME = "customerName";

	@Autowired
	CustomerService customerService;

	protected void doAction() throws CommandException {

		HashCodeGenerator hashCode = new HashCodeGenerator();
		String email = getRequestParameter(EMAIL);
		String password = getRequestParameter(PASSWORD);

		Customer customer = null;

		customer = customerService.takeByEmailPassword(email, (hashCode.createHashedValue(password)));
		System.out.println(hashCode.createHashedValue(password));
		if (customer != null) {
			setSessionAttribute(CUSTOMER_ROLE_ID, String.valueOf(customer.getRoleId()));
			setSessionAttribute(CUSTOMER_ID, String.valueOf(customer.getCustomerId()));
			setSessionAttribute(CUSTOMER_NAME, customer.getFirstName());

			messageToJson(MessageEnum.INFO_SUCCESSFUL_LOGIN, true);
		} else {
			removeAttribute(CUSTOMER_ROLE_ID);

			messageToJson(MessageEnum.ERROR_INCORRECT_PASSWORD_OR_EMAIL, false);
		}
	}

	@Override
	protected boolean dataValidate() {
		return DataValidator.EMAIL.check(getRequestParameter(EMAIL))
				&& DataValidator.PASSWORD.check(getRequestParameter(PASSWORD));
	}

	@Override
	protected void errorDataMessage() {
		messageToJson(MessageEnum.ERROR_INCORRECT_PASSWORD_OR_EMAIL, false);
	}

	@Override
	protected boolean roleValidate() {
		// if role not exits
		boolean value = false;
		if (takeRoleId() == null) {
			value = true;
		}
		return value;
	}

	@Override
	protected void errorRoleMessage() {
		messageToJson(MessageEnum.INFO_ALREADY_LOGIN, false);

	}
}