package com.epam.theatre.action.event;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Event;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.EventService;
import com.epam.theatre.util.JsonConvertor;

@Component("event-all")
public class EventAllAction extends AbstractAction {

	@Autowired
	EventService eventService;

	@Override
	protected void doAction() throws CommandException {
		List<Event> events;

		events = eventService.getAll();

		JsonConvertor<Event> jsonConv = new JsonConvertor<>();
		setJsonResult(jsonConv.toJson(events));
	}

	@Override
	protected boolean dataValidate() {
		// no data for validate
		return true;
	}

	@Override
	protected boolean roleValidate() {
		return true;
	}
}