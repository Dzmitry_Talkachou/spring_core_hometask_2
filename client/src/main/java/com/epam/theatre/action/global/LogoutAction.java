package com.epam.theatre.action.global;

import org.springframework.stereotype.Component;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.manager.MessageEnum;

@Component("logout")
public class LogoutAction extends AbstractAction {

	@Override
	protected void doAction() {
		sessionInvalidate();
		messageToJson(MessageEnum.INFO_SUCCESSFUL_LOGOUT, true);

	}

	@Override
	protected boolean dataValidate() {
		boolean value = false;
		if (takeRoleId() != null) {
			value = true;
		}
		return value;
	}

	@Override
	protected boolean roleValidate() {
		// all roles
		return true;
	}

	@Override
	protected void errorDataMessage() {
		messageToJson(MessageEnum.INFO_ALREADY_LOGIN, false);
	}
}