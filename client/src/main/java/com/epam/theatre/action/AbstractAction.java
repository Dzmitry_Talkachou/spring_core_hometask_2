package com.epam.theatre.action;

import javax.servlet.http.HttpServletRequest;

import com.epam.theatre.exception.CommandException;
import com.epam.theatre.manager.MessageEnum;
import com.epam.theatre.manager.MessageManager;
import com.epam.theatre.util.JsonConvertor;
import com.epam.theatre.util.ServerAnswer;
import com.epam.theatre.util.ServerMessage;


/**
 * The Class AbstractCommand.
 */
public abstract class AbstractAction implements Action {

	/** The request. */
	private HttpServletRequest request;

	/** The json result. */
	private String jsonResult;

	/** The server answer. */
	private ServerAnswer serverAnswer = new ServerAnswer();

	/** The Constant LOCALE. */
	private static final String LOCALE = "locale";

	/** The Constant CUSTOMER_ROLE_ID. */
	private static final String CUSTOMER_ROLE_ID = "roleId";

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customerId";


	private void setRequest(HttpServletRequest request) {
		this.request = request;
	}


	private void setMessage(String message, boolean value) {
		this.serverAnswer.setMessage(message, value);
	}


	protected String getJsonResult() {
		return jsonResult;
	}


	protected void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}


	protected String takeJsonMessage() {
		return takeJsonMessage(serverAnswer.getMessage());
	}


	final public String execute(HttpServletRequest request) throws CommandException {

		setRequest(request);
		String json = null;

		if (this.roleValidate()) {
			
			if (this.dataValidate()) {
				this.doAction();
				
			} else {
				this.errorDataMessage();
			}
		} else {
			this.errorRoleMessage();
		}
		
		json = getJsonResult();
		
		if (json == null) {
			noItemMessage();
			json = getJsonResult();
		}
		
		return json;
	}

	/**
	 * Do action.
	 *
	 * @throws CommandException
	 *             the command exception
	 */
	protected abstract void doAction() throws CommandException;

	/**
	 * Data validate.
	 *
	 * @return true, if successful
	 */
	protected abstract boolean dataValidate();

	/**
	 * Role validate.
	 *
	 * @return true, if successful
	 */
	protected abstract boolean roleValidate();

	/**
	 * Error role message.
	 */
	protected void errorRoleMessage() {

		messageToJson(MessageEnum.ERROR_ACCESS_DENIED, false);
	}

	/**
	 * Error data message.
	 */
	protected void errorDataMessage() {

		messageToJson(MessageEnum.ERROR_WRONG_DATA, false);
	}

	/**
	 * No item message.
	 */
	protected void noItemMessage() {
		messageToJson(MessageEnum.ERROR_NO_ITEM_FOUND, false);
	}

	/**
	 * Message to json.
	 *
	 * @param message
	 *            the message
	 * @param value
	 *            the value
	 */
	protected void messageToJson(MessageEnum message, boolean value) {
		setMessage(MessageManager.getProperty(message, takeLocale()), value);
		setJsonResult(takeJsonMessage());
	}

	/**
	 * Sets the session attribute.
	 *
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	protected void setSessionAttribute(String name, String value) {
		this.request.getSession().setAttribute(name, value);

	}

	/**
	 * Gets the session attribute.
	 *
	 * @param name
	 *            the name
	 * @return the session attribute
	 */
	protected String getSessionAttribute(String name) {
		return (String) this.request.getSession().getAttribute(name);
	}

	/**
	 * Gets the request parameter.
	 *
	 * @param name
	 *            the name
	 * @return the request parameter
	 */
	protected String getRequestParameter(String name) {
		return this.request.getParameter(name);

	}

	protected void removeAttribute(String name) {
		this.request.getSession().removeAttribute(name);

	}

	protected void sessionInvalidate() {
		this.request.getSession().invalidate();
	}

	/**
	 * Take locale.
	 *
	 * @return the string
	 */
	protected String takeLocale() {
		String locale = null;
		if (this.request.getSession().getAttribute(LOCALE) != null) {
			locale = (String) this.request.getSession().getAttribute(LOCALE);
		}
		return locale;
	}

	/**
	 * Take role id.
	 *
	 * @return the string
	 */
	protected String takeRoleId() {
		return (String) this.request.getSession().getAttribute(CUSTOMER_ROLE_ID);
	}

	/**
	 * Take customer id.
	 *
	 * @return the string
	 */
	protected String takeCustomerId() {
		return (String) this.request.getSession().getAttribute(CUSTOMER_ID);
	}

	/**
	 * Successful create message.
	 */
	protected void successfulCreateMessage() {
		messageToJson(MessageEnum.INFO_SUCCESSFUL_CREATE, true);
	}

	/**
	 * Successful delete message.
	 */
	protected void successfulDeleteMessage() {
		messageToJson(MessageEnum.INFO_SUCCESSFUL_DELETE, true);
	}

	/**
	 * Unsuccessful delete message.
	 */
	protected void unsuccessfulDeleteMessage() {
		messageToJson(MessageEnum.INFO_UNSUCCESSFUL_DELETE, true);
	}

	/**
	 * Successful update message.
	 */
	protected void successfulUpdateMessage() {
		messageToJson(MessageEnum.INFO_SUCCESSFUL_UPDATE, true);
	}

	/**
	 * Unsuccessful update message.
	 */
	protected void unsuccessfulUpdateMessage() {
		messageToJson(MessageEnum.INFO_UNSUCCESSFUL_UPDATE, false);
	}

	/**
	 * Unsuccessful create message.
	 */
	protected void unsuccessfulCreateMessage() {
		messageToJson(MessageEnum.INFO_UNSUCCESSFUL_CREATE, false);
	}

	/**
	 * Successful find message.
	 */
	protected void sussefulFindMessage() {
		messageToJson(MessageEnum.INFO_SUCCESSFUL_FIND, true);
	}

	/**
	 * Take json message.
	 *
	 * @param message
	 *            the message
	 * @return the string
	 */
	protected String takeJsonMessage(ServerMessage message) {
		JsonConvertor<ServerMessage> jsonConv = new JsonConvertor<>();
		return jsonConv.toJson(message);
	}
}