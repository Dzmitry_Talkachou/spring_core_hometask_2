package com.epam.theatre.action.customer;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.theatre.action.AbstractAction;
import com.epam.theatre.domain.Customer;
import com.epam.theatre.exception.CommandException;
import com.epam.theatre.service.CustomerService;
import com.epam.theatre.validation.DataValidator;

public class CustomerUpdateAction extends AbstractAction {

	/** The Constant CUSTOMER_ID. */
	private static final String CUSTOMER_ID = "customer-id";

	/** The Constant FIRST_NAME. */
	private static final String FIRST_NAME = "first-name";

	/** The Constant SECOND_NAME. */
	private static final String SECOND_NAME = "second-name";
//
//	/** The Constant PAYMENT_METHOD_ID. */
//	private static final String PAYMENT_METHOD_ID = "payment-method-id";

	/** The Constant CUSTOMER_NAME. */
	private static final String CUSTOMER_NAME = "customerName";

	@Autowired
	CustomerService customerService;

	@Override
	protected void doAction() throws CommandException {
		Customer customer = new Customer();

		try {

			Long customerId = Long.valueOf(getRequestParameter(CUSTOMER_ID));
			customer.setFirstName(getRequestParameter(FIRST_NAME));
			customer.setLastName(getRequestParameter(SECOND_NAME));
			// customer.setPaymentMethodId(Long.valueOf(getRequestParameter(PAYMENT_METHOD_ID)));

			customerService.updateById(customerId, customer);
			setSessionAttribute(CUSTOMER_NAME, customer.getFirstName());

			successfulUpdateMessage();

		} catch (NumberFormatException e) {
			throw new CommandException("CommandException in CustomerUpdateCommand class", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.nyair.command.AbstractCommand#dataValidate()
	 */
	@Override
	protected boolean dataValidate() {
		return DataValidator.ID.check(getRequestParameter(CUSTOMER_ID))
				&& DataValidator.FIRST_NAME.check(getRequestParameter(FIRST_NAME))
				&& DataValidator.SECOND_NAME.check(getRequestParameter(SECOND_NAME));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see by.epam.nyair.command.AbstractCommand#roleValidate()
	 */
	@Override
	protected boolean roleValidate() {

		return takeCustomerId().equals(getRequestParameter(CUSTOMER_ID));
	}
}