package com.epam.theatre.constant;

/**
 * The Class LocaleConstant.
 */
public class LocaleConstant {

	/** The Constant RU_BY. */
	public static final String RU_BY = "ru_BY";
	
	/** The Constant EN_GB. */
	public static final String EN_GB = "en_GB";

}
