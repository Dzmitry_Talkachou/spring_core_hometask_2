package com.epam.theatre.manager;

/**
 * The Enum MessageEnum.
 */
public enum MessageEnum {

	/** The info successful update. */
	INFO_SUCCESSFUL_UPDATE("info.successful.update"),

	/** The info successful delete. */
	INFO_SUCCESSFUL_DELETE("info.successful.delete"),

	/** The info successful create. */
	INFO_SUCCESSFUL_CREATE("info.successful.create"),

	/** The info unsuccessful update. */
	INFO_UNSUCCESSFUL_UPDATE("info.unsuccessful.update"),

	/** The info successful find. */
	INFO_SUCCESSFUL_FIND("info.successful.find"),

	/** The server error. */
	SERVER_ERROR("server.error"),

	/** The error wrong data. */
	ERROR_WRONG_DATA("error.wrong.data"),

	/** The server error null. */
	SERVER_ERROR_NULL("server.error.null"),

	/** The error incorrect password or email. */
	ERROR_INCORRECT_PASSWORD_OR_EMAIL("error.incorrect.password.or.email"),

	/** The error no item found. */
	ERROR_NO_ITEM_FOUND("error.no.item.found"),

	/** The info incorrect id. */
	INFO_INCORRECT_ID("info.successful.login"),

	/** The info successful login. */
	INFO_SUCCESSFUL_LOGIN("info.successful.login"),

	/** The info already login. */
	INFO_ALREADY_LOGIN("info.already.login"),

	/** The error access denied. */
	ERROR_ACCESS_DENIED("error.access.denied"),

	/** The info successful logout. */
	INFO_SUCCESSFUL_LOGOUT("info.successful.logout"),

	/** The info already logout. */
	INFO_ALREADY_LOGOUT("info.already.logout"),

	/** The info english locale. */
	INFO_ENGLISH_LOCALE("info.english.locale"),

	/** The info russian locale. */
	INFO_RUSSIAN_LOCALE("info.russian.locale"),

	/** The info error locale. */
	INFO_ERROR_LOCALE("info.error.locale"),

	/** The plane status OK. */
	PLANE_STATUS_OK("plane.status.ok"),

	/** The plane status repair. */
	PLANE_STATUS_REPAIR("plane.status.repair"),

	/** The schedule status canceled. */
	SCHEDULE_STATUS_CANCELED("schedule.status.canceled"),

	/** The schedule status done. */
	SCHEDULE_STATUS_DONE("schedule.status.done"),

	/** The schedule status work. */
	SCHEDULE_STATUS_WORK("schedule.status.work"),

	/** The schedule status planned. */
	SCHEDULE_STATUS_PLANNED("schedule.status.planned"),

	/** The role admin. */
	ROLE_ADMIN("role.admin"),

	/** The role manager. */
	ROLE_MANAGER("role.manager"),

	/** The role customer. */
	ROLE_CUSTOMER("role.customer"),

	/** The page option add bags. */
	PAGE_OPTION_ADD_BAGS("page.option.add.bags"),

	/** The page option choose seat. */
	PAGE_OPTION_CHOOSE_SEAT("page.option.choose.seat"),

	/** The page option get insurance. */
	PAGE_OPTION_GET_INSURANCE("page.option.get.insurance"),

	/** The page option sports equipment. */
	PAGE_OPTION_SPORTS_EQUIPMENT("page.option.sports.equipment"),

	/** The page option music equipment. */
	PAGE_OPTION_MUSIC_EQUIPMENT("page.option.music.equipment"),

	/** The page option baby equipment. */
	PAGE_OPTION_BABY_EQUIPMENT("page.option.baby.equipment"),

	/** The page option parking. */
	PAGE_OPTION_PARKING("page.option.parking"),

	/** The page euro. */
	PAGE_EURO("page.euro"),

	/** The page business type ticket. */
	PAGE_BUSINESS_TYPE_TICKET("page.business.type.ticket"),

	/** The info unsuccessful create. */
	INFO_UNSUCCESSFUL_CREATE("info.unsuccessful.create"),

	/** The error wrong email. */
	ERROR_WRONG_EMAIL("error.wrong.email"),

	/** The error change role. */
	ERROR_CHANGE_ROLE("error.role.change"),

	/** The info unsuccessful delete. */
	INFO_UNSUCCESSFUL_DELETE("info.unsuccessful.delete"), 

	/** The thank you for purchasing. */
	THANK_YOU_FOR_PURCHASING("thank.you.for.purchasing"),
	
	/** The tickets for this flight is no more. */
	TICKETS_FOR_THIS_FLIGHT_IS_NO_MORE("tickets.for.this.flight.is.no.more");

	/** The message. */
	private final String message;

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Instantiates a new message enum.
	 *
	 * @param message
	 *            the message
	 */
	MessageEnum(String message) {
		this.message = message;
	}

	/**
	 * From string.
	 *
	 * @param message
	 *            the message
	 * @return the message enum
	 */
	public static MessageEnum fromString(String message) {
		MessageEnum value = null;
		for (MessageEnum messageEnum : MessageEnum.values()) {
			if (message.equalsIgnoreCase(messageEnum.message)) {
				value = messageEnum;
			}
		}
		return value;
	}
}