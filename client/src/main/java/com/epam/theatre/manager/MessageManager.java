package com.epam.theatre.manager;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.theatre.constant.LocaleConstant;

/**
 * The Class MessageManager.
 */
public class MessageManager {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(MessageManager.class);

	/** The Constant resourceBundleRuBy. */
	private static final ResourceBundle resourceBundleRuBy = ResourceBundle.getBundle("messages",
			new Locale("ru", "BY"));

	/** The Constant resourceBundleEnGb. */
	private static final ResourceBundle resourceBundleEnGb = ResourceBundle.getBundle("messages", Locale.UK);

	/**
	 * Instantiates a new message manager.
	 */
	private MessageManager() {
	}

	/**
	 * Gets the property.
	 *
	 * @param key
	 *            the key
	 * @param localeType
	 *            the locale type
	 * @return the property
	 */
	public static String getProperty(MessageEnum key, String localeType) {

		String value = null;

		if (localeType == null) {
			localeType = LocaleConstant.EN_GB;
		}

		switch (localeType) {

		case LocaleConstant.RU_BY:

			value = resourceBundleRuBy.getString(key.getMessage());
			break;

		case LocaleConstant.EN_GB:
			value = resourceBundleEnGb.getString(key.getMessage());
			break;

		default:
			LOGGER.log(Level.INFO, "Unknown locale: " + localeType);

		}
		return value;
	}
}