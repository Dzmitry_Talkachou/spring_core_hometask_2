package com.epam.theatre.manager;

import java.util.ResourceBundle;

/**
 * The Class ConfigManager.
 */
public class ConfigManager {

	/** The Constant resourceBundle. */
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

	/**
	 * Instantiates a new config manager.
	 */
	private ConfigManager() {
	}

	/**
	 * Gets the property.
	 *
	 * @param key the key
	 * @return the property
	 */
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}