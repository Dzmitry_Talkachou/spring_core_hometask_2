define(['jquery'], function($) {

  var writeArrivalCity = {

    createTable: function(responseText, replace) {

      $(replace).text('');
      $.each(responseText.ArrayList, function() {
        $(replace).append(
                '<div class="city-name" id="arrival-city-menu" data-route-id="'
                        + this.routeId + '" >' + this.arrivalCityName
                        + '</div>');

      });
      $(replace).show();

    }

  };
  return writeArrivalCity;

});
