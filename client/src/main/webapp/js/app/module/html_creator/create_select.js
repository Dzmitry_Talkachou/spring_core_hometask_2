define(['jquery'], function($) {

  var createSelect = {

    create: function(json, replace, selectId, selectName, itemId, itemName,
            selectedId) {

      $(replace).html('');

      var select = '<select id="' + selectId + '" name="' + selectName + '">';

      $.each(json.ArrayList, function() {
        var selected = "";

        if (this[itemId] == selectedId) {
          selected = ' selected ';
        }
        
        if (this.planeStatusId == undefined || this.planeStatusId != 2) {

          select += '<option  ' + selected + ' value="' + this[itemId] + '" >'

          + this[itemName] + '</option>';

        }

      });

      select += '</select>';
      $(replace).html(select);

    }
  };

  return createSelect;

});