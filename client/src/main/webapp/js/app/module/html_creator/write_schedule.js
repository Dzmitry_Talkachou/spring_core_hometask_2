define(['jquery', 'jqcookie', 'md/date_localization'], function($, jqcookie,
        dateLocalization) {

  var writeSchedule = {

    createTable: function(responseText, replace) {

      var departureCityName = '';
      var arrivalCityName = '';

      $(replace).text('');
      $.each(responseText.ArrayList, function() {

        if (this.scheduleStatusId != "3" && this.scheduleStatusId != "1") {

          departureCityName = this.departureCityName;

          arrivalCityName = this.arrivalCityName;

          $(replace).append(
                  '<div class="schedule-date"' + ' data-schedule-id="'
                          + this.scheduleId + '"'
                          + ' data-departure-city-name="'
                          + this.departureCityName + '"'
                          + ' data-ticket-cost="' + this.ticketCost + '"'
                          + ' data-business-ticket-cost="'
                          + this.businessTicketCost
                          + '" data-arrival-city-name="' + this.arrivalCityName
                          + '"' + ' data-route-name="' + this.routeName + '"'
                          + ' data-schedule-id="' + this.scheduleId + '"'
                          + ' data-departure-time="' + this.departureDate.hour
                          + ':' + this.departureDate.minute + '"'
                          + ' data-arrival-time="' + this.departureDate.hour
                          + ':' + this.departureDate.minute + '"'
                          + ' data-flight-time="' + this.flightTime.hour + ':'
                          + this.flightTime.minute + '"'

                          + '>' + '<span class="schedule-day-of-week">'
                          + dateLocalization.dayOfWeek(this.departureDate.dayOfWeek)
                          + '<span class="schedule-cost">€' + this.ticketCost
                          + '</span>' + '<span class="schedule-date">'
                          + this.departureDate.dayOfMonth + ' '
                          + dateLocalization.month(this.departureDate.month) + ' ' + this.departureDate.year +  '</span>'
                          
          
          );
          
       
        }
      });
      $(replace).show();

      $('#cart-tab-route-cities').text(
              departureCityName + ' - ' + arrivalCityName);

    },

    createBigTable: function() {

      $(document).on(
              'click',
              '.schedule-date',
              function() {

                $.removeCookie('scheduleId');

                $('.cart-tab').hide();

                $('.cart-cost').text(0);

                $('.schedule-date').css('background', '');
                $('.ticket-cost').css('background', '');

                $(this).css('background',
                        'linear-gradient(to top, #fff, #d4eafb, #fff)');

                $('#schedule-concrete').show();

                var scheduleId = $(this).data('scheduleId');
                var routeName = $(this).data('routeName');
                var arrivalCityName = $(this).data('arrivalCityName');
                var departureCityName = $(this).data('departureCityName');
                var departureTime = $(this).data('departureTime');
                var arrivalTime = $(this).data('arrivalTime');
                var ticketCost = $(this).data('ticketCost');
                var businessTicketCost = $(this).data('businessTicketCost');
                var flightTime = $(this).data('flightTime');

                var arrivalTimeArray = arrivalTime.split(':');
                var flightTimeArray = flightTime.split(':');

                var nullMin1 = '';

                if (parseInt(flightTimeArray[1]) < 10) {
                  nullMin1 = '0';
                }

                flightTime = flightTimeArray[0] + ":" + nullMin1
                        + flightTimeArray[1];

                var mins = Number(arrivalTimeArray[1])
                        + Number(flightTimeArray[1]);
                var minhrs = Math.floor(parseInt(mins / 60));
                var hrs = Number(arrivalTimeArray[0])
                        + Number(flightTimeArray[0]) + minhrs;
                mins = mins % 60;

                var nullMin = '';

                if (mins < 10) {
                  nullMin = '0';
                }
                
                if (hrs > 23) {hrs = hrs - 23; }
                

                var arrivalTimeNew = hrs + ':' + nullMin + mins;

                $('#sch-schedule-id').val(scheduleId);
                $('#sch-ticket-cost-standart').text('€ ' + ticketCost);
                $('#sch-ticket-cost-business').text('€ ' + businessTicketCost);
                $('#sch-route-name').text(routeName);
                $('#sch-route-depart-city').text(departureCityName);
                $('#sch-route-arrival-city').text(arrivalCityName);
                $('#sch-depart-time').text(departureTime);
                $('#sch-arrival-time').text(arrivalTimeNew);
                $('#sch-flight-time').text(flightTime);
              });
    }

  };
  return writeSchedule;

});
