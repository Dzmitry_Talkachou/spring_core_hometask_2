define([ 'jquery', 'html/create_schedule_event_by_id' ], function($,
		createSheduleEventById) {

	var selectSchedule = {

		action : function() {
			$(document).on('click', '.select-schedule', function() {

				$(".schedule-container").hide();

				createSheduleEventById.create(this.id);

			});
		}
	}
	return selectSchedule;

});