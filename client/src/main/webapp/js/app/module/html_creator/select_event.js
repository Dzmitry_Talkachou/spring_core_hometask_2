define([ 'jquery', 'html/create_schedule_event' ], function($,
		createSheduleEvent) {

	var selectEvent = {

		action : function() {
			$(document).on('click', '.select-event', function() {

				$(".event-container").hide();

				createSheduleEvent.create(this.id);

			});
		}
	}
	return selectEvent;

});