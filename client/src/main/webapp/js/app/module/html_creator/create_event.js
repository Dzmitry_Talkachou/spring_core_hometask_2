define([ 'jquery' ], function($) {

	var createEvent = {

		create : function() {

			$(".event-container").text('');

			$.ajax({
				url : 'controller',
				data : {

					command : 'event-all'
				},
				success : function(json) {

					if (json.ServerMessage == undefined) {

						$.each(json.ArrayList, function() {
						
			

							$(".event-container").append(
									'<div class="event"><em>' + this.eventName
											+ '</em><em>' + this.rating
											+ '</em><input class="select-event" id="'+ this.eventId + '" type="button" value="select"/>' + this.rating
											+ '</em></div>');

						});

					}

				}

			});
		}
	};
	return createEvent;

});