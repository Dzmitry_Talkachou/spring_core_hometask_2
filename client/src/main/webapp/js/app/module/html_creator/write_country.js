define(['jquery', 'html/create_select'], function($, createSelect) {

  var writeCountry = {

    createTable: function(responseText, replace) {
      $(replace).text('');
      $.each(responseText.ArrayList, function() {
        $(replace).append(

                '<div class="country-name" data-country-id="' + this.countryId
                        + '" >' + this.countryName + '</div>');

      });
      $(replace).show();
    },
    createSelect: function(replace, selectName, selectId, selectedId) {

      $.ajax({
        url: 'controller',
        data: {

          command: 'country-all',
          locale: $(this).data('locale')
        },
        success: function(json) {

          if (json.ServerMessage == undefined) {

            createSelect.create(json, replace, selectId, selectName,
                    'countryId', 'countryName', selectedId);

          }

        }

      });

    }

  };
  return writeCountry;

});