define(['jquery', 'jqrcode'], function($, jqrcode) {

  var writeTicketOptions = {

    create: function(customerId, ticketId) {

      $.ajax({
        type: 'POST',
        url: 'controller',
        data: {
          'ticket-id': ticketId,
          'customer-id': customerId,
          command: 'ticket-options-by-ticket-id'

        },
        success: function(json) {

          if (json.ServerMessage == undefined) {

            if (json.ArrayList != undefined) {

              $('#ticket-option-list').html('<div class="option-list-small">');

              $.each(json.ArrayList, function(key, value) {

                $('#ticket-option-list').append(
                        '<img class="option-small" src="images/product-'
                                + value.optionName + '.png" >');

              });

              $('#ticket-option-list').append('</div>');

            }
          }
        }
      });

    }
  };

  return writeTicketOptions;

});
