define(['jquery', 'html/create_select'], function($, createSelect) {

  var writeCity = {

    createTable: function(json, replace) {
      $(replace).text('');
      $.each(json.ArrayList, function() {
        $(replace).append(
                '<div class="city-name" id="departure-city-menu" data-city-id="'
                        + this.cityId + '" >' + this.cityName + '</div>');

      });
      $(replace).show();
    },
    createSelect: function(replace, selectName, selectId, selectedId) {

      $.ajax({
        url: 'controller',
        data: {

          command: 'city-all'
        },
        success: function(json) {

          if (json.ServerMessage == undefined) {

            createSelect.create(json, replace, selectId, selectName, 'cityId',
                    'cityName', selectedId);

          }

        }

      });

    }
  };

  return writeCity;

});