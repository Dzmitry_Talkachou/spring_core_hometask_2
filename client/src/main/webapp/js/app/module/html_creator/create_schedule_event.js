define([ 'jquery' ], function($) {

	var createScheduleEvent = {

		create : function(eventId) {
			
			$(".schedule-container").text('');
			$(".schedule-container").show();

			$.ajax({
				url : 'controller',
				data : {

					command : 'schedule-by-event-id',
					'event-id':eventId
				},
				success : function(json) {

					if (json.ServerMessage == undefined) {

						$.each(json.ArrayList, function() {
							console.log(this);

							$(".schedule-container").append(
									'<div class="schedule"><em>' + this.eventDate
											+ '</em><em>' + this.auditoriumId
											+ '</em><input class="select-schedule" id="'+ this.eventScheduleId + '" type="button" value="select"/>' + this.rating
											+ '</em></div>');

						});

					}

				}

			});
		}
	};
	return createScheduleEvent;

});