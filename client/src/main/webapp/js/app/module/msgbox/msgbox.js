define(['jquery'], function($) {

  var msgBox = {

    init: function(msgBox) {

      $(document).on('click', '#close-button-msgbox-' + msgBox, function() {

        $('#' + $(this).data('msgboxId')).remove();

      });
    },

    create: function(msgBoxId, message, result) {

      var msgBoxMgs;

      msgBoxMgs = '<div class="message-box-' + result + '" id="' + msgBoxId
              + '" ><div class="close-button-msgbox" id="close-button-msgbox-'
              + msgBoxId + '" data-msgbox-id="' + msgBoxId
              + '" ><img  id="close-button-msgbox"'
              + 'src="images/close_button_tab.png"></div><span>' + message
              + '</span></div>';

      $('#server-message').append(msgBoxMgs);

      msgBox.init(msgBoxId);

    },

    serverMsg: function(msgBoxId, json) {

      if (json.ServerMessage != undefined) {

        msgBox.create(msgBoxId, json.ServerMessage.message,
                json.ServerMessage.result);

      }

      console.log(json);

    },

    show: function(msgBoxId) {

      $('.'.msgBoxId).show();

    },

    hide: function(msgBoxId) {

      $('.'.msgBoxId).hide();

    },
    destroy: function(msgBoxId) {

      $('.'.msgBoxId).remove();

    }

  };

  return msgBox;

});