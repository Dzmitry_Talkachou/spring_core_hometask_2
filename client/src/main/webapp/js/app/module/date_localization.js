define(['jquery'], function($) {

  var dateLocalization = {

    dayOfWeek: function(dayOfWeek) {

      var locale = $('#locale-info').data('locale');

      var localizedDayOfWeek;

      if (locale == "en_GB" || locale == "") {

        switch (dayOfWeek) {

        case "SUNDAY":

          localizedDayOfWeek = "Sunday";

          break;
        case "MONDAY":

          localizedDayOfWeek = "Monday";

          break;

        case "TUESDAY":

          localizedDayOfWeek = "Tuesday";

          break;
        case "WEDNESDAY":

          localizedDayOfWeek = "Wednesday";

          break;
        case "THURSDAY":

          localizedDayOfWeek = "Thursday";

          break;
        case "FRIDAY":

          localizedDayOfWeek = "Friday";

          break;

        case "SATURDAY":

          localizedDayOfWeek = "Saturday";

          break;

        }

      }

      if (locale == "ru_BY") {

        switch (dayOfWeek) {

        case "SUNDAY":

          localizedDayOfWeek = "Воскресенье";

          break;
        case "MONDAY":

          localizedDayOfWeek = "Понедельник";

          break;

        case "TUESDAY":

          localizedDayOfWeek = "Вторник";

          break;
        case "WEDNESDAY":

          localizedDayOfWeek = "Среда";

          break;
        case "THURSDAY":

          localizedDayOfWeek = "Четверг";

          break;
        case "FRIDAY":

          localizedDayOfWeek = "Пятница";

          break;

        case "SATURDAY":

          localizedDayOfWeek = "Суббота";

          break;

        }

      }

      return localizedDayOfWeek;
    },

    month: function(month) {

      var locale = $('#locale-info').data('locale');

      var localizedMonth;

      if (locale == "en_GB" || locale == "") {

        switch (month) {

        case "JANUARY":

          localizedMonth = "Jan";

          break;
        case "FEBRUARY":

          localizedMonth = "Feb";

          break;

        case "MARCH":

          localizedMonth = "Mar";

          break;
        case "APRIL":

          localizedMonth = "Apr";

          break;
        case "MAY":

          localizedMonth = "May";

          break;
        case "JUNE":

          localizedMonth = "Jun";

          break;

        case "JULY":

          localizedMonth = "Jul";

          break;
        case "AUGUST":

          localizedMonth = "авг";

          break;
        case "SEPTEMBER":

          localizedMonth = "Aug";

          break;

        case "OCTOBER":

          localizedMonth = "Oct";

          break;

        case "NOVEMBER":

          localizedMonth = "Nov";

          break;
        case "DECEMBER":

          localizedMonth = "Dec";

          break;

        }
      }

      if (locale == "ru_BY") {

        switch (month) {

        case "JANUARY":

          localizedMonth = "янв";

          break;
        case "FEBRUARY":

          localizedMonth = "фев";

          break;

        case "MARCH":

          localizedMonth = "мар";

          break;
        case "APRIL":

          localizedMonth = "апр";

          break;
        case "MAY":

          localizedMonth = "май";

          break;
        case "JUNE":

          localizedMonth = "июн";

          break;

        case "JULY":

          localizedMonth = "июл";

          break;
        case "AUGUST":

          localizedMonth = "авг";

          break;
        case "SEPTEMBER":

          localizedMonth = "сен";

          break;

        case "OCTOBER":

          localizedMonth = "окт";

          break;

        case "NOVEMBER":

          localizedMonth = "ноя";

          break;
        case "DECEMBER":

          localizedMonth = "дек";

          break;

        }
      }

      return localizedMonth;
    }

  };

  return dateLocalization;

});