define(['jquery'], function($) {

  var clearBuyTicketForm = {

    clear: function() {

      $('#schedule-id-buy-ticket').val('');
      $('#route-cities-buy-ticket').text('');
      $('#departure-date-buy-ticket').text('');
      $('#departure-time-buy-ticket').text('');
      $('#plane-name-buy-ticket').text('');
      $('#tail-number-buy-ticket').text('');
      $('.option-checkbox').removeAttr('checked');
      $('.buy-ticket-chk').css('color', '#000').css('background-color',
              '#fafaf6').css('border', '1px #efefef solid');

    }
  };

  return clearBuyTicketForm;

});