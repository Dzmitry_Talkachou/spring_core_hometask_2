define(['jquery', 'html/create_select', 'admin/write_table',
    'admin/form_action', 'admin/send_form'], function($, createSelect,
        writeTable, formAction, sendForm) {

  var takeAll = {

    take: function(name, command) {

      $.ajax({
        url: 'controller',
        data: {
          command: command
        },
        success: function(responseText) {

          writeTable.create(name, responseText);

          $(document).on('click', '.table-line', function() {

            var id = $(this).attr('data-' + name + '-id');

            $('.table-line').css('background', "#f4f4f4");
            $('.table-line').css('color', "#000");

            $(this).css('background', "#3399FF").css('color', "#f4f4f4");

            formAction.fillForm(name, id);

            $('#' + name + '-create').click(function() {

              var command = $(this).attr('id');

              sendForm.send(name, command);

            pause(1000);

            });

            $(document).on('click', '#' + name + '-update', function() {

              var command = $(this).attr('id');

              if (name == 'customer') {

                command = "customer-role-change";
              }

              sendForm.send(name, command);

               pause(1000);

            });

            $(document).on('click', '#' + name + '-delete', function() {

              var command = $(this).attr('id');

              sendForm.send(name, command);

               pause(1000);

            });

            $(document).on('click', '.button-close', function() {

              formAction.close(name);

            });

          });

        }
      });
    }

  };

  return takeAll;

});