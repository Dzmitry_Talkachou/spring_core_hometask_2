define(['jquery', 'jqcookie', 'admin/write_route_button', 'admin/write_table','md/field_action'],
        function($, jqcookie, writeRouteButton, writeTable,fieldAction) {

          var writeTable = {

            create: function(name, responseText) {
              
              fieldAction.valid('input[type="text"]');

              var editorList = '#' + name + '-list';

              var beginTag = '<div class="table-line" data-' + name + '-id="';
              var endTag = '</div>';
              var tagOpen = '<div class="table-ceil">';
              var tagOpenSmall = '<div class="table-ceil-small">';
              var tagOpenMid = '<div class="table-ceil-mid">';
              var tagClose = '</div>';

              $(editorList).text('');

              switch (name) {
              case 'city':

                $.each(responseText.ArrayList, function() {
                  $(editorList).append(
                          beginTag + this.cityId + '">' + tagOpenSmall
                                  + this.cityId + tagClose + tagOpen
                                  + this.cityName + tagClose + tagOpen
                                  + this.countryName + tagClose + endTag);
                });

                break;

              case 'country':

                $.each(responseText.ArrayList, function() {
                  $(editorList).append(
                          beginTag + this.countryId + '">' + tagOpenSmall
                                  + this.countryId + tagClose + tagOpen
                                  + this.countryName + tagClose + endTag);
                });
                break;

              case 'route':

                $.each(responseText.ArrayList, function() {
                  $(editorList).append(
                          beginTag + this.routeId + '">' + tagOpenSmall
                                  + this.routeId + tagClose + tagOpen
                                  + this.departureCityName + tagClose + tagOpen
                                  + this.arrivalCityName + tagClose + tagOpen
                                  + this.distance + tagClose + tagOpen
                                  + this.routeName + tagClose + endTag);
                });

                break;

              case 'schedule':

                writeRouteButton.create();

                $.each(responseText.ArrayList, function() {

                  if ($.cookie('routeId') == undefined
                          || $.cookie('routeId') == this.routeId) {
                    $(editorList).append(
                            beginTag + this.scheduleId + '">' + tagOpenSmall
                                    + this.scheduleId + tagClose + tagOpen
                                    + this.departureDate.dayOfMonth + "-"
                                    + this.departureDate.monthValue + "-"
                                    + this.departureDate.year + " "
                                    + this.departureDate.hour + ":"
                                    + this.departureDate.minute + tagClose
                                    + tagOpen + this.tailNumber + tagClose
                                    + tagOpen + this.routeName + tagClose

                                    + tagOpenMid + ' <img src="images/'
                                    + this.scheduleStatusName + '.png" >'

                                    + tagClose + tagOpen + this.ticketsQuantity
                                    + tagClose + tagOpen + this.ticketsSold
                                    + tagClose + tagOpen + this.revenue
                                    + tagClose

                                    + endTag);
                  }
                });

                break;

              case 'plane':

                $.each(responseText.ArrayList, function() {
                  $(editorList).append(
                          beginTag + this.planeId + '">' + tagOpenSmall
                                  + this.planeId + tagClose + tagOpen
                                  + this.modelName + tagClose + tagOpen
                                  + this.tailNumber + tagClose + tagOpenMid
                                  + ' <img src="images/' + this.planeStatusName
                                  + '.png" >' + endTag);
                });

                break;

              case 'model':

                $.each(responseText.ArrayList,
                        function() {
                          $(editorList).append(
                                  beginTag + this.modelId + '">' + tagOpenSmall
                                          + this.modelId + tagClose + tagOpen
                                          + this.modelName + tagClose + tagOpen
                                          + this.speed + tagClose + tagOpen
                                          + this.fuelConsumption + tagClose
                                          + tagOpen + this.passengerCapacity
                                          + tagClose + endTag);
                        });

                break;

              case 'customer':

                $.each(responseText.ArrayList, function() {
                  $(editorList).append(
                          beginTag + this.customerId + '">' + tagOpenSmall
                                  + this.customerId + tagClose + tagOpen
                                  + this.firstName + tagClose + tagOpen
                                  + this.secondName + tagClose

                                  + tagOpenMid + ' <img src="images/'
                                  + this.roleName + '.png" >' +

                                  tagClose + endTag);
                });

                break;

              default:

                $('.console').show();
              }
            }

          };

          return writeTable;

        });