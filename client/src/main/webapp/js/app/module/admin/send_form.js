define(['jquery', 'admin/take_all', 'admin/write_table','md/field_action'], function($, takeAll,
        writeTable,fieldAction) {

  var sendForm = {

    send: function(name, command) {

      var msg = $('#' + name + '-form').serialize() + '&command=' + command;

      $.ajax({
        type: 'POST',
        url: 'controller',
        data: msg,
        success: function(data) {

          if (data.ServerMessage.message != undefined) {
            $('.console').text(data.ServerMessage.message);

          } else {
            $('.console').text("Unknown server error.");
          }

          $('.console').show();

          $('form').hide();

          var command;

          if ($.cookie('routeId') != undefined && name == 'schedule') {

            command = 'command=schedule-by-route-id&route-id='
                    + parseInt($.cookie('routeId'));

          } else {

            command = 'command=' + name + '-all';

          }

          $.ajax({
            url: 'controller',
            data: command,
            success: function(responseText) {

              writeTable.create(name, responseText);
              
              fieldAction.valid('input[type="text"]');

            }
          });

        },
        error: function(xhr, str) {

          $('.console').html(xhr.responseText);
          $('.console').show();
          $('form').hide();

        }
      });
    }

  };

  return sendForm;

});