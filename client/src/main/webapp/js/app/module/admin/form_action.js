define(
        ['jquery', 'html/write_city', 'html/write_country','html/create_select','md/field_action'],
        function($, writeCity, writeCountry, createSelect,fieldAction) {

          var formAction = {

            close: function(name) {

              $('#' + name + '-form').hide();
              $('#' + name + '-form').trigger('reset');
              $('.table-line').css('background', "#f4f4f4");
              $('.table-line').css('color', "#000");

            },

            fillForm: function(name, id) {
              
              fieldAction.valid('input[type="text"]');
              
              $('em').hide();
              $('.star').show();

              $('#' + name + '-form').show().draggable();

              var command = name + '-by-id';

              var msg = 'command=' + command + '&' + name + '-id=' + id;

              $
                      .ajax({
                        url: 'controller',
                        data: msg,
                        success: function(json) {

                          switch (name) {
                          case 'city':

                            $('#' + name + '-city-id').val(json.City.cityId);
                            $('#' + name + '-city-name')
                                    .val(json.City.cityName);
                            $('#' + name + '-country-id').val(
                                    json.City.countryId);

                            writeCountry.createSelect('#city-country-select',
                                    'country-id"', 'city-country-id',
                                    json.City.countryId);

                            break;

                          case 'country':

                            $('#' + name + '-country-id').val(
                                    json.Country.countryId);
                            $('#' + name + '-country-name').val(
                                    json.Country.countryName);

                            break;

                          case 'route':
                            $('#' + name + '-route-id').val(
                                    json.FlightRoute.routeId);
                            $('#' + name + '-departure-city-id').val(
                                    json.FlightRoute.departureCityId);
                            $('#' + name + '-arrival-city-id').val(
                                    json.FlightRoute.arrivalCityId);
                            $('#' + name + '-distance').val(
                                    json.FlightRoute.distance);
                            $('#' + name + '-route-name').val(
                                    json.FlightRoute.routeName);

                            writeCity.createSelect('#route-depat-city-select',
                                    'departure-city-id',
                                    'route-departure-city-id',
                                    json.FlightRoute.departureCityId);

                            writeCity.createSelect(
                                    '#route-arrival-city-select',
                                    'arrival-city-id', 'route-arrival-city-id',
                                    json.FlightRoute.arrivalCityId);

                            break;

                          case 'schedule':
                            $('#' + name + '-schedule-id').val(
                                    json.FlightSchedule.scheduleId);
                            $('#' + name + '-departure-date')
                                    .val(
                                            json.FlightSchedule.departureDate.dayOfMonth
                                                    + "-"
                                                    + json.FlightSchedule.departureDate.monthValue
                                                    + "-"
                                                    + json.FlightSchedule.departureDate.year
                                                    + " "
                                                    + json.FlightSchedule.departureDate.hour
                                                    + ":"
                                                    + json.FlightSchedule.departureDate.minute);

                            $('#' + name + '-plane-id').val(
                                    json.FlightSchedule.planeId);
                            $('#' + name + '-route-id').val(
                                    json.FlightSchedule.routeId);
                            $('#' + name + '-schedule-status-id').val(
                                    json.FlightSchedule.scheduleStatusId);
                            $('#' + name + '-kets-sold').val(
                                    json.FlightSchedule.tiketsSold);
                            $('#' + name + '-revenue').val(
                                    json.FlightSchedule.revenue);

                            $.ajax({
                              url: 'controller',
                              data: {
                                command: 'plane-all'
                              },
                              success: function(json1) {

                                createSelect.create(json1,
                                        '#schedule-plane-select', 'plane-id',
                                        'schedule-plane-id', 'planeId',
                                        'tailNumber',
                                        json.FlightSchedule.planeId);
                              }

                            });

                            $.ajax({
                              url: 'controller',
                              data: {
                                command: 'route-all'
                              },
                              success: function(json1) {

                                createSelect.create(json1,
                                        '#route-plane-select', 'route-id',
                                        'schedule-route-id', 'routeId',
                                        'routeName',
                                        json.FlightSchedule.routeId);

                              }
                            });

                            break;

                          case 'plane':
                            $('#' + name + '-plane-id').val(json.Plane.planeId);
                            $('#' + name + '-model-id').val(json.Plane.modelId);
                            $('#' + name + '-tail-number').val(
                                    json.Plane.tailNumber);
                            $('#' + name + '-plane-status-id').val(
                                    json.Plane.planeStatusId);

                            $.ajax({
                              url: 'controller',
                              data: {
                                command: 'model-all'
                              },
                              success: function(json1) {

                                createSelect.create(json1,
                                        '#plane-model-select',
                                        'plane-model-id', 'model-id',
                                        'modelId', 'modelName',
                                        json.Plane.modelId);

                              }
                            });

                            break;

                          case 'model':
                            $('#' + name + '-model-id').val(
                                    json.PlaneModel.modelId);
                            $('#' + name + '-model-name').val(
                                    json.PlaneModel.modelName);
                            $('#' + name + '-speed').val(json.PlaneModel.speed);
                            $('#' + name + '-fuel-consumption').val(
                                    json.PlaneModel.fuelConsumption);
                            $('#' + name + '-passenger-capacity').val(
                                    json.PlaneModel.passengerCapacity);

                            break;

                          case 'customer':

                            console.log(json.Customer.customerId);

                            $('#' + name + '-customer-id').val(
                                    json.Customer.customerId);

                            $('#' + name + '-old-role-id').val(
                                    json.Customer.roleId);

                            $(
                                    '#' + name + '-role-id [value="'
                                            + json.Customer.roleId + '"]')
                                    .attr("selected", "selected");

                            break;

                          default:
                            $('.console').text('Item not found: ' + name);
                            $('.console').show();
                          }

                        }
                      });

            }

          };

          return formAction;

        });