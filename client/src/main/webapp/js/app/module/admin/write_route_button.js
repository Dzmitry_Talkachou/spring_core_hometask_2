define(['jquery', 'jqcookie', 'html/create_select', 'admin/write_table'],
        function($, jqcookie, createSelect, writeTable) {

          var writeRouteButton = {

            create: function() {

              var msg = 'command=route-unique-with-schedule';

              $.ajax({
                url: 'controller',
                data: msg,
                success: function(json) {

                  createSelect.create(json, '#schedule-route-buttons',
                          'route-select-schedule', '', 'routeId', 'routeName',
                          $.cookie('routeId'));

                }

              });

            }

          };
          return writeRouteButton;

        });
