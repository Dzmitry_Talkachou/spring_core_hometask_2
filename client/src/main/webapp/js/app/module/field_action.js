define(['jquery'], function($) {

  var fieldAction = {

    error: function(name) {

      if ($(name).val() == '') {

        $(name).css('border', '1px #d2d6d9 solid');

      } else {

        $(name).css('border', '1px red solid');

      }

    },
    valid: function(name) {

      $(name).css('border', '1px #d2d6d9 solid');
      $(name+'-warning').hide();
    }
  };

  return fieldAction;

});