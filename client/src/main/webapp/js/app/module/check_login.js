define(['jquery'], function($) {

  var checkLogin = {

    check: function() {

      $.ajax({
        url: 'controller',
        data: {
          command: 'login-customer-info'
        },
        success: function(json) {

          if (json.ServerMessage == undefined) {

            if (json.Customer != undefined) {

              $('#mynyair-menu-mynyair').trigger('click');

            } else {

              $('#button-my-nyair').trigger('click');

            }

          } else {


            $('#button-my-nyair').trigger('click');

          }
        }

      });

    }

  };

  return checkLogin;

});
