define(['jquery'], function($) {

  var departureCityClickOnLoad = {

    action: function() {

      $('.departure-menu').show();
      $('.cities').hide();


      $('.departure-city').data('cityId', '').css('color', '#ССС');
      $('.arrival-city').css('color', '#ССС').text('').data('cityId', '');

    }
  };
  return departureCityClickOnLoad;

});