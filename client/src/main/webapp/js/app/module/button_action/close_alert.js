define(['jquery'], function($) {

  var closeAlert = {

    action: function() {

      $(document).on('click', '#alert-danger-close-button', function() {



        $('#message-login').slideUp(100, function() {
          $('.menu-dropdown').css('height', '338px');
          $('.col-featuers').css('height', '287px');
        }).data('view', 'false');

      });
      $(document).on('click', '#alert-danger-close-button-register',
              function() {

                $('#message-register').slideUp(100, function() {
                  $('.menu-dropdown').css('height', '554px');
                  $('.col-featuers').css('height', '504px');

                }).data('view', 'false');

              });
    }
  };

  return closeAlert;

});