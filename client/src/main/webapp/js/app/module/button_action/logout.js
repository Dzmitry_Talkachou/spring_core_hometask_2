define(['jquery'], function($) {

  var logout = {

    action: function() {

      $(document).on('click', '#mynyair-menu-logout', function() {
        
       console.log();

        $('.settings-tab').hide();
        $('.mynyair-tab').hide();

        $.ajax({
          url: 'controller',
          data: {
            command: 'logout'
          },
          success: function(json) {

            if (json.ServerMessage != undefined) {

              if (json.ServerMessage.result = true) {
                
               

                $.removeCookie('business');
                $.removeCookie('scheduleId');
                $.removeCookie('ticketCost');
                $.removeCookie('destination');

                window.location.reload();
              }

            }

          }

        });

      });
    }
  };

  return logout;

});
