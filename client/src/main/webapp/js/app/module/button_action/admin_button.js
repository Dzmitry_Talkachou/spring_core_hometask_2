define(['jquery'], function($) {

  var adminButton = {

    create: function() {

      $('#mynyair-menu-admin').unbind().show();
      $('.mynyair-menu-dropdown').css('height', '145px');

      $(document).on('click', '#mynyair-menu-admin', function() {

        $.removeCookie('scheduleId');
        $.removeCookie('business');
        $.removeCookie('ticketCost');

        window.location = '/nyair/admin';

      });

    },
    remove: function() {

      $('#mynyair-menu-admin').hide().unbind();
      $('.mynyair-menu-dropdown').css('height', '110px');

    }
  };
  return adminButton;
});