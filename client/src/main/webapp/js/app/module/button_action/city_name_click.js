define(['jquery'], function($) {

  var cityNameClick = {

    deparuteCity: function() {

      $(document).on(
              'click',
              '#departure-city-menu',
              function() {

                var cityId = $(this).data('cityId');

                $('.city-name').css('background', '#FFF');

                $(this).css('background', '#FFCC33');

                $('#departure-cities, #departure-countries').hide();

                $('.departure-city').data('error', 'false')
                        .css('color', '#000').data('cityId', cityId).text(
                                $(this).text());
                
                $('.arrival-city').trigger('click');

              });

    },

    arrivalCity: function() {

      $(document).on('click', '#arrival-city-menu', function() {

        var routeId = $(this).data('routeId');

        $('.city-name').css('background', '#FFF');

        $(this).css('background', '#FFCC33');

        if ($(this).data('routeId') == undefined) {

          $('.arrival-city').text($(this).text());

        }

        $('.arrival-city').data('routeId', routeId);

        $('.arrival-city').css('color', '#000');
        $('.arrival-city').data('error', 'false');

      });

    }

  };

  return cityNameClick;

});