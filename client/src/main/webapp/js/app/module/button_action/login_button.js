define(['jquery'], function($) {

  var loginButton = {

    action: function() {

      $(document).on('click', '#login-button', function() {

        var email = $('#login-field').val();
        var password = $('#password-field').val();

        $.ajax({
          type: 'POST',
          url: 'controller',
          data: {
            command: 'login',
            'email': email,
            'password': password
          },
          success: function(json) {

            if (json.ServerMessage != undefined) {

              if (json.ServerMessage.result == false) {

                $('#message-login span').text(json.ServerMessage.message);

                $('#message-login').data('view', 'true');

                $('#message-login').slideDown(100, function() {

                  $('.menu-dropdown').css('height', '419px');
                  $('.col-featuers').css('height', '369px');
                });

              } else {

                window.location.reload();

              }

            } else {

              $('#message-login span').text(json.ServerMessage.message);

              $('#message-login').data('view', 'true');

              $('#message-login').slideDown(100, function() {

                $('.menu-dropdown').css('height', '419px');
                $('.col-featuers').css('height', '369px');
              });

            }
          }
        });
      });

    }
  };

  return loginButton;

});