define(['jquery'], function($) {

  var buttonAction = {

    disable: function(name) {

      $(name).prop("disabled", true);
      $(name).css('background-color', '#ddd');
      $(name).css('color', '#aaa');

    },
    enable: function(name) {

      $(name).removeAttr('disabled');
      $(name).css('background-color', '#F1C933');
      $(name).css('color', '#073590');
     

    }
  };

  return buttonAction;

});