define(['jquery', 'ba/admin_button'], function($, adminButton) {

  var myNyairButton = {

    action: function() {

      $('#button-my-nyair').data('clickValue', false);

      $(document).on('click', '#button-my-nyair', function() {
        
        $('.ticket-option-list').hide();
        $('#password-field').val('');
        $('#login-field').val('');

        $('#message-login').hide().data('view', false);

        var myNyair = '';
        var customerId = '';
        var customerRoleId = '';

        $.ajax({
          url: 'controller',
          data: {
            command: 'login-customer-info'

          },
          success: function(json) {

            console.log(json);

            if (json.ServerMessage == undefined) {

              if (json.Customer != undefined) {

                customerId = json.Customer.customerId;
                customerRoleId = json.Customer.roleId;

                myNyair = '';

                if (customerId != null && customerId != undefined) {

                  myNyair = 'mynyair-';

                  if (customerRoleId == 5) {

                    adminButton.create();

                  } else {

                    adminButton.remove();

                  }

                }

              }
            }

            if ($('#button-my-nyair').data('clickValue') == false) {

              $('#button-my-nyair').data('clickValue', true);

              $('.' + myNyair + 'menu-dropdown').slideDown(100, function() {

                $('.' + myNyair + 'menu-dropdown').show();

              });

            } else {

              $('#button-my-nyair').data('clickValue', false);

              $('.menu-dropdown').slideUp(100, function() {

                $('.' + myNyair + 'menu-dropdown').hide();

              });

              $('#message-login').data('view', false);
              $('#password-field').val('');
              $('#login-field').val('');

            }

          }

        });

      });

    }
  };

  return myNyairButton;

});