define(['jquery', 'ba/departure_city_click_onload', 'html/write_city',
    'html/write_country', 'ba/button_action'], function($,
        departureCityClickOnLoad, writeCity, writeCountry, buttonAction) {

  var departureCityClick = {

    action: function() {

      $('.departure-city').on('click', function() {
       
        buttonAction.disable('#continue-schedule');

        $('.arrival-menu').hide();
        $('.datepicker-menu').hide();
        $('.departure-city').text('');

        $.ajax({
          url: 'controller',
          data: {
            command: 'country-all'
          },
          success: function(responseText) {

            writeCountry.createTable(responseText, '#departure-countries');

            departureCityClickOnLoad.action();

            $('.country-name').click(function() {



              var countryId = $(this).data('countryId');

              $('.country-name').css('color', '#000').css('background', '#FFF');

              $(this).css('background', '#3399FF');
              $(this).css('color', '#FFF');

              $.ajax({
                url: 'controller',
                data: {
                  command: 'city-by-country-id',
                  'country-id': countryId
                },
                success: function(responseText) {

                  writeCity.createTable(responseText, '#departure-cities');

                  $('.cities').css('height', $('.countries').css('height'));

                }
              });

            });

          }
        });

      });

    }
  };
  return departureCityClick;

});