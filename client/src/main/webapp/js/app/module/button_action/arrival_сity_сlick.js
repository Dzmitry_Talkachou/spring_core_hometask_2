define(['jquery', 'html/write_arrival_city', 'ba/button_action'], function($,
        writeArrivalCity, buttonAction) {

  var arrivalCityClick = {

    action: function() {

      $('.arrival-city').on(
              'click',

              function() {

                $('.departure-menu').hide();
                $('.arrival-menu').show();

                $('.arrival-city').data('cityId', '').text('');

                var departureCityId = $('.departure-city').data('cityId');

                if ($('.departure-city').data('error') != true
                        || $('.departure-city').data('cityId') != undefined) {

                  $.ajax({
                    url: 'controller',
                    data: {
                      command: 'route-by-departure-city-id',
                      'departure-city-id': departureCityId
                    },
                    success: function(responseText) {

                      writeArrivalCity.createTable(responseText,
                              '#arrival-cities');

                      $('.city-name').click(
                              function() {

                                $(this).css('background', '#FFCC33');

                                $('.arrival-city').css('color', '#000').text($(this).text());
                                $('.arrival-city').data('routeId', $(this)
                                        .data('routeId'));

                                buttonAction.enable('#continue-schedule');
                              });

                    }

                  });
                }
              });

    }
  };

  return arrivalCityClick;

});
