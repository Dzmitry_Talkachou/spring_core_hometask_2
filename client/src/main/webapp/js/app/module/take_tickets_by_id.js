define(['jquery', 'jqrcode', 'html/write_ticket_option',
    'md/fill_buy_ticket_form'], function($, jqrcode, writeTicketOption,
        fillBuyTicketForm) {

  var takeTicketsById = {

    action: function() {

      $.ajax({
        url: 'controller',
        data: {
          command: 'login-customer-info'

        },
        success: function(json) {

          if (json.ServerMessage == undefined) {

            if (json.Customer != undefined) {

              var customerId = json.Customer.customerId;

              var msg = 'command=ticket-all-by-customer-id&customer-id='
                      + json.Customer.customerId;

              $.ajax({
                type: "POST",
                url: 'controller',
                data: msg,
                success: function(json) {

                  if (json.ArrayList != undefined) {

                    $.each(json.ArrayList, function() {

                      $('#ticket-list').append(
                              '<div class="qrcode" id="qrcode-id-'
                                      + this.ticketId
                                      + '" style="display:none;"></div>');

                      $('#qrcode-id-' + this.ticketId).qrcode({
                        "size": 130,
                        "color": "#3a3",
                        "text": this.ticketKey
                      });

                      $('#ticket-list').append(
                              '<div class="ticket-line"  data-ticket-cost="'
                                      + this.ticketCost + '" data-ticket-id="'
                                      + this.ticketId + '" data-schedule-id="'
                                      + this.scheduleId + '"><div>'
                                      + this.routeName + '</div><div>'
                                      + this.departureCityName + '-'
                                      + this.arrivalCityName + '</div><div>€ '
                                      + this.ticketCost + '</div><div>'
                                      + this.departureDate.dayOfMonth + '-'
                                      + this.departureDate.monthValue + '-'
                                      + this.departureDate.year + '</div><div>'
                                      + this.departureDate.hour + ":"
                                      + this.departureDate.minute
                                      + '</div></div>');

                    });

                  }

                }

              });

              $(document).on(
                      'click',
                      '.ticket-line',
                      function() {

                        $('.ticket-line').css('background-color', '#fff');
                        $('.ticket-line').css('color', '#000');

                        var thisTicketLine = $(this);

                        if ($(this).data('scheduleId') != undefined) {

                          var ticketId = $(this).data('ticketId');

                          $('#buy-last-buy').hide();

                          $.ajax({
                            url: 'controller',
                            data: {
                              command: 'schedule-by-id',
                              'schedule-id': $(this).data('scheduleId')
                            },
                            success: function(json) {

                              fillBuyTicketForm.fill(json);

                              $('.ticket-list').css('top', '295px');

                              if ($.cookie('business') == 'true') {
                                $('#total-cost-buy-ticket').text(
                                        $(thisTicketLine).data(
                                                'businessTicketCost'));
                                $('#business-include').hide();

                              } else {
                                $('#total-cost-buy-ticket').text(
                                        $(thisTicketLine).data('ticketCost'));
                                $('#business-include').show();
                              }

                              $(thisTicketLine).css('background-color',
                                      '#073590');
                              $(thisTicketLine).css('color', '#f0c833');

                              $('#buy-ticket-tab').show();

                              $('.e-ticket-msg').show();

                              $('.buy-edit').hide();

                              $('.qrcode').hide();

                              $('#qrcode-id-' + ticketId).show();

                              writeTicketOption.create(customerId, ticketId);
                              $('.ticket-option-list').show();

                            }

                          });

                        }
                      });

            }

          }
        }
      });

    }
  };

  return takeTicketsById;

});