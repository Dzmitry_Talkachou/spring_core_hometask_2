define(['jquery'], function($) {

  var languageSelect = {

    action: function() {

      $('.language-select').on('click', function() {

        $.ajax({
          url: 'controller',
          data: {

            command: 'change-locale',
            locale: $(this).data('locale')
          },
          success: function(json) {

            if (json.ServerMessage != undefined) {

              if (json.ServerMessage.result = true) {

                window.location.reload();
              }

            }

          }
        });
      });

    }
  };
  return languageSelect;

});