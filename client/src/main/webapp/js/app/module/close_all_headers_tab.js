define(['jquery'], function($, jqrcode) {

  var closeAllHeadersTab = {

    closeAllTab: function() {

      $('#button-my-nyair').data('clickValue', 'false');
      $('.flag-cont').data('clickValue', 'false');
      $('.mynyair-menu-dropdown').hide();
      $('.menu-dropdown').hide();
      $('.language-menu-dropdown').hide();

    },

    closeWindows: function() {

    }
  };

  return closeAllHeadersTab;

});