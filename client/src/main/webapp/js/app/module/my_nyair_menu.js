define(['jquery', 'jqcookie', 'md/take_tickets_by_id',
    'md/clear_buy_ticket_form', 'md/fill_buy_ticket_form'], function($,
        jqcookie, takeTicketsById, clearBuyTicketForm, fillBuyTicketForm) {

  var myNyairMenu = {

    action: function() {

      $(document).on('click', '#mynyair-menu-mynyair', function() {
        
        $('.ticket-option-list').hide();

        $('.settings-tab ,.e-ticket-msg ,.buy-ticket').hide();

        $('.ticket-list').css('top', '10px');

        clearBuyTicketForm.clear();

        $('.mynyair-tab').show();

        $('#ticket-list').text('');

        $('#button-my-nyair').data('clickValue', false);

        $('.menu-dropdown').slideUp(100, function() {

          $('.mynyair-menu-dropdown').hide();

        });

        if ($.cookie('scheduleId') != undefined) {

          $('#buy-last-buy').hide();

          $.ajax({
            url: 'controller',
            data: {
              command: 'schedule-by-id',
              'schedule-id': $.cookie('scheduleId')
            },
            success: function(json) {

              $('.option-list-small').hide();
              $('#buy-ticket-tab, #edit-buy-ticket').show();
              

              fillBuyTicketForm.fill(json);

            }

          });
        } else {

          takeTicketsById.action();

        }

      });

    }
  };

  return myNyairMenu;

});