define(['jquery'], function($) {

  var fillBuyTicketForm = {

    fill: function(json) {

      $('#schedule-id-buy-ticket').val(json.FlightSchedule.scheduleId);
      $('#route-name-buy-ticket').text(json.FlightSchedule.routeName);
      $('#route-cities-buy-ticket').text(
              json.FlightSchedule.departureCityName + ' - '
                      + json.FlightSchedule.arrivalCityName);

      $('#departure-date-buy-ticket').text(

              json.FlightSchedule.departureDate.dayOfMonth + "-"
                      + json.FlightSchedule.departureDate.monthValue + "-"
                      + json.FlightSchedule.departureDate.year);

      $('#departure-time-buy-ticket').text(
              +json.FlightSchedule.departureDate.hour + ":"
                      + json.FlightSchedule.departureDate.minute);

      $('#plane-name-buy-ticket').text(json.FlightSchedule.modelName);
      $('#tail-number-buy-ticket').text(json.FlightSchedule.tailNumber);

      if ($.cookie('business') == 'true') {
     

        $('#option-business').val("on");

        $('#total-cost-buy-ticket')
                .text(json.FlightSchedule.businessTicketCost);
        $('#business-include').hide();
      } else {
        $('#option-business').val("");
        $('#total-cost-buy-ticket').text(json.FlightSchedule.ticketCost);
        $('#business-include').show();
      }

    }
  };

  return fillBuyTicketForm;

});