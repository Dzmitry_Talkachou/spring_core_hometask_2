define(['jquery', 'vl/data_validator'], function($, dataValidator) {

  var fieldsValidation = {
    action: function() {

      $(document).on('keyup', '#login-field', function() {

        dataValidator.email($(this).attr('id'), '#login-button');

      });
      
      $(document).on('keyup', '#password-field', function() {

        dataValidator.password($(this).attr('id'), '#login-button');

      });

      $(document).on('keyup', '#customer-second-name', function() {

        dataValidator.firstName($(this).attr('id'), '#customer-register');

      });

      $(document).on('keyup', '#customer-first-name', function() {

        dataValidator.secondName($(this).attr('id'), '#customer-register');

      });

      $(document).on('keyup', '#customer-email', function() {

        dataValidator.email($(this).attr('id'), '#customer-register');

      });

      $(document).on('keyup', '#customer-password-tab', function() {

        dataValidator.password($(this).attr('id'), '#customer-register');

      });
    
      $(document).on('keyup', '#tab-customer-first-name', function() {

        dataValidator.firstName($(this).attr('id'), '#tab-customer-update');

      });

      $(document).on('keyup', '#tab-customer-second-name', function() {

        dataValidator.secondName($(this).attr('id'), '#tab-customer-update');

      });

      $(document).on('keyup', '#tab-old-password', function() {

        dataValidator.password($(this).attr('id'), '#tab-customer-password-update');

      });

      $(document).on('keyup', '#tab-customer-password', function() {

        dataValidator.password($(this).attr('id'), '#tab-customer-password-update');

      });
      
      $(document).on('keyup', '#plane-tail-number', function() {

        dataValidator.tailNumber($(this).attr('id'), '#plane-create, #plane-update, #plane-delete');

      });
      
      $(document).on('keyup', '#model-model-name', function() {

        dataValidator.modelName($(this).attr('id'), '#model-create, #model-update, #model-delete');

      });
      
      $(document).on('keyup', '#model-speed', function() {

        dataValidator.speed($(this).attr('id'), '#model-create, #model-update, #model-delete');

      });
      
      $(document).on('keyup', '#model-fuel-consumption', function() {

        dataValidator.fuelConsumption($(this).attr('id'), '#model-create, #model-update, #model-delete');

      });
      
      $(document).on('keyup', '#model-passenger-capacity', function() {

        dataValidator.passengerCapacity($(this).attr('id'), '#model-create, #model-update, #model-delete');

      });
      
      
      $(document).on('keyup', '#schedule-departure-date', function() {

        dataValidator.date($(this).attr('id'), '#schedule-create, #schedule-update, #schedule-delete');

      });
      
      $(document).on('keyup', '#route-distance', function() {

        dataValidator.distance($(this).attr('id'), '#route-create, #route-update, #route-delete');

      });
      
      $(document).on('keyup', '#route-route-name', function() {

        dataValidator.routeName($(this).attr('id'), '#route-create, #route-update, #route-delete');

      });
      
      $(document).on('keyup', '#country-country-name', function() {

        dataValidator.countryName($(this).attr('id'), '#country-create, #country-update, #country-delete');

      });
      
      $(document).on('keyup', '#city-city-name', function() {

        dataValidator.cityName($(this).attr('id'), '#city-create, #city-update, #city-delete');

      });
      
      

    }
  };

          return fieldsValidation;

        });