define(
        ['jquery', 'ba/button_action', 'md/field_action'],
        function($, buttonAction, fieldAction) {

          var dataValidator = {

            //comapre regex and checkField, if checkField valid - errorCounter on submit button decrease by 1, 
            //if errorCounter on submit button == 0 then enable button, else disable button
                  
            compare: function(regex, checkField, button) {

              
              //error counter
              var errorCounter;

              checkField = '#' + checkField;

              if ($(checkField).val() == '') {

                $(checkField + '-warning').hide();

              }

              if ($(checkField).data('valid') == undefined) {

                $(checkField).data('valid', true);

              }

              if ($(button).data('errorCounter') != undefined) {

                errorCounter = parseInt($(button).data('errorCounter'));

              } else {

                errorCounter = 0;
              }

              if (regex.test($(checkField).val())) {

                $(checkField + '-warning').hide();

                if ($(checkField).data('valid') == false) {

                  $(checkField).data('valid', true);

                  errorCounter--;

                }

                fieldAction.valid(checkField);

              } else {

                if ($(checkField).val() != '') {
                  $(checkField + '-warning').show();
                }

                if (errorCounter == undefined) {

                  errorCounter = 0;

                }

                if ($(checkField).data('valid') == true) {

                  $(checkField).data('valid', false);

                  errorCounter++;

                }

                buttonAction.disable(button);

                fieldAction.error(checkField);

              }

              if (errorCounter <= 0) {

                buttonAction.enable(button);

                errorCounter = 0;
              }

              $(button).data('errorCounter', errorCounter);

            },

            number: function(checkField, button) {
              var regex = /^0$|^[1-9]\d{0,7}$/;
              dataValidator.compare(regex, checkField, button);
            },
            id: function(checkField, button) {

              var regex = /^[1-9]\d{0,7}$/;
              dataValidator.compare(regex, checkField, button);
            },
            firstName: function(checkField, button) {

              var regex = /^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F]{1,25}$/;

              dataValidator.compare(regex, checkField, button);

            },
            secondName: function(checkField, button) {

              var regex = /^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F]{1,25}$|^[A-Z][a-z']{1,25}-[A-Z][a-z']{2,25}$|^[\u0410-\u042F][\u0430-\u044F]{1,25}-[\u0410-\u042F][\u0430-\u044F]{2,25}$/;

              dataValidator.compare(regex, checkField, button);

            },
            cityName: function(checkField, button) {
              var regex = /^[A-Z][a-z']{1,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}$|^[A-Z][a-z']{1,25}[-|\s][A-Z][a-z']{2,25}$|^[\u0410-\u042F][\u0430-\u044F']{1,25}-[\u0410-\u042F][\u0430-\u044F']{2,25}$/;
              dataValidator.compare(regex, checkField, button);
            },
            countryName: function(checkField, button) {
              var regex = /^[A-Z]{2,5}$|^[\u0410-\u042F]$|^[A-Z]([a-z']){1,60}$|^[\u0410-\u042F]([\u0430-\u044F']){1,60}$|^([A-Z]([a-z']){1,25}(-[A-Z][a-z']|\\s[A-Z][a-z']){0,1}([a-z']){0,20})(-[A-Z][a-z']|\s[A-Z][a-z'])([a-z']){0,20}$|^([\u0410-\u042F]([\u0430-\u044F']){1,25}(-[\u0410-\u042F]|\\s[\u0410-\u042F]){0,1}([\u0430-\u044F']){0,20})(-[\u0410-\u042F]|\s[\u0410-\u042F])([\u0430-\u044F']){0,20}$/;
              dataValidator.compare(regex, checkField, button);
            },
            modelName: function(checkField, button) {
              var regex = /^[A-Z][a-z'-']{1,20}[\s][A-Za-z'0-9-]{1,15}[A-Za-z'0-9]{1,15}$|^[\u0410-\u042F][\u0430-\u044F'-']{1,20}[\s][\u0410-\u042F\u0430-\u044F'0-9-|A-Za-z'0-9-]{1,15}[\u0410-\u042F\u0430-\u044F'0-9|A-Za-z'0-9]{1,15}$/;
              dataValidator.compare(regex, checkField, button);

            },
            email: function(checkField, button) {

              var regex = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;

              dataValidator.compare(regex, checkField, button);

            },

            date: function(checkField, button) {
              var regex = /^([1-9]|[1-2][0-9]|3[0-1])-([1][0-2]|[1-9])-(201[6-9]|203[0-7])\s(([2][0-3]|[1][0-9]|[1-9]):([0-5][0-9]))$/;
              dataValidator.compare(regex, checkField, button);
            },

            password: function(checkField, button) {

              var regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
              dataValidator.compare(regex, checkField, button);

            },

            tailNumber: function(checkField, button) {

              var regex = /^SU-[1-2]\d{3}$/;
              dataValidator.compare(regex, checkField, button);

            },

            fuelConsumption: function(checkField, button) {
              var regex = /^[5-9]\d{2}$|^[1-4]\d{3}$/;
              dataValidator.compare(regex, checkField, button);
            },

            passengerCapacity: function(checkField, button) {
              var regex = /^[2-9]\d{1}$|^[1-9]\d{2}$/;
              dataValidator.compare(regex, checkField, button);
            },

            routeName: function(checkField, button) {
              var regex = /^[A-Z][A-Z]-[1-9]\d{3}$/;
              dataValidator.compare(regex, checkField, button);
            },
            speed: function(checkField, button) {
              var regex = /^[2-9]\d{2}$|^[1]\d{3}$/;
              dataValidator.compare(regex, checkField, button);
            },

            distance: function(checkField, button) {
              var regex = /^[1-9]\d{2}$|^[1-9]\d{3}$|^[1]\d{4}$/;
              dataValidator.compare(regex, checkField, button);
            }

          };
          return dataValidator;

        });
