define(['jquery', 'msg/msgbox'], function($, msgbox) {

  var settingMenu = {

    action: function() {

      $(document).on(
              'click',
              '#mynyair-menu-settings',
              function() {

                $('.settings-tab').show();
                $('.mynyair-tab').hide();

                var customerId = '';

                $.ajax({
                  url: 'controller',
                  data: {
                    command: 'login-customer-info'
                  },
                  success: function(customer) {

                    $.each(customer, function() {

                      if (this.customerId != "null"
                              && this.customerId != undefined) {

                        customerId = this.customerId;

                        $.ajax({
                          type:'POST',
                          url: 'controller',
                          data: {
                            command: 'customer-by-id',
                            'customer-id': customerId
                          },
                          success: function(json) {

                            $.each(json, function() {

                              if (this.customerId != "null"
                                      && this.customerId != undefined) {

                                $('#tab-customer-first-name').val(
                                        this.firstName);

                                $('#tab-customer-second-name').val(
                                        this.secondName);

                                $('#tab-customer-id').val(this.customerId);
                                
                                $('#tab-customer-id-pass').val(this.customerId);

                                $(
                                        "#tab-customer-payment-method-id [value='"
                                                + this.paymentMethodId + "']")
                                        .attr("selected", "selected");

                              }

                            });

                          }
                        });
                      }
                    });

                  }
                });

                $('.menu-dropdown').slideUp(100, function() {

                  $('#button-my-nyair').data('clickValue', 'false');

                  $('.mynyair-menu-dropdown').hide();

                });

              });

    }
  };

  return settingMenu;

});
