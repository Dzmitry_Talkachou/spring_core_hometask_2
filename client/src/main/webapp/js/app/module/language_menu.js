define(['jquery'], function($) {

  var languageMenu = {

    action: function() {

      $(document).on('click', '.flag-cont', function() {

        if ($(this).data('clickValue') == false) {

          $(this).data('clickValue', true);

          $('.language-menu-dropdown').slideDown(100, function() {
            
            $(this).show();

          });

        } else {

          $(this).data('clickValue', false);
          
          $('.language-menu-dropdown').slideUp(100, function() {
            
            $(this).hide();

          });

        }

      });
    }
  };

  return languageMenu;

});