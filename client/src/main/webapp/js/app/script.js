define(['jquery', 'jquery.mousewheel', 'jqcookie', 'ba/my_nyair_button',
    'md/language_menu', 'ba/login_button', 'ba/close_alert', 'ba/logout',
    'ba/departure_city_click', 'html/create_event', 'html/write_city', 'ba/arrival_сity_сlick',
    'html/write_schedule', 'md/language_select', 'ba/city_name_click',
    'md/check_login', 'md/setting_menu', 'md/my_nyair_menu',
    'md/close_all_headers_tab', 'md/take_tickets_by_id', 'ba/button_action',
    'md/field_action', 'vl/data_validator', 'vl/fields_validation',
    'msg/msgbox', 'admin/take_all', 'admin/write_table','html/select_event','html/select_schedule'], function($,
        mousewheel, jqcookie, myNyairButton, languageMenu, loginButton,
        closeAlert, logout, departureCityClick, createEvent,  writeCity, arrivalCityClick,
        writeSchedule, languageSelect, cityNameClick, checkLogin, settingMenu,
        myNyairMenu, closeAllHeadersTab, takeTicketsById, buttonAction,
        fieldAction, dataValidator, fieldsValidation, msgBox, takeAll,
        writeTable, selectEvent,selectSchedule) {

  var main = {

    start: function() {

      // header
      
      createEvent.create();
     
  		
  		selectEvent.action();
  
  		selectSchedule.action();
  		
      // MyNyair menu button event listener activate
      myNyairButton.action();

      // Language menu event listener activate
      languageMenu.action();

      // Language menu selection event listener activate
      languageSelect.action();

      // Login button action event listener activate
      loginButton.action();

      // Logout command event listener activate
      logout.action();

      // Close alert event listener activate
      closeAlert.action();

      // my nyair pages

      // Setting menu event listener activate
      settingMenu.action();

      // MyNyair menu event listener activate
      myNyairMenu.action();

      // Fields validation event listener activate
      fieldsValidation.action();

      // select route

      // Departure city click event listener activate
      cityNameClick.deparuteCity();
      departureCityClick.action();

      // Arrival city click event listener activate
      cityNameClick.arrivalCity();
      arrivalCityClick.action();

      // disable all buttons (for validator)
      buttonAction.disable('.core-btn-primary');

      // enable buttons who not needed for validator
      buttonAction.enable('#tab-customer-update');
      buttonAction.enable('#continue-buy-ticket');
      buttonAction.enable('#buy-ticket');
      buttonAction.enable('#buy-ticket-cancel');

      // if JavaScript enable hide warning message
      $('#no-js').hide();

      // disable right button mouse click
      $('body').on('contextmenu', function(e) {
        return false;
      });

      // hide ticket option - business plus (fake option needed for ticket type
      // business plus)
      $('#bussines-ticket').hide();

      // if cookie scheduleId exist, then enable cart and put there ticket
      if ($.cookie('scheduleId') != undefined) {
        $('.cart-cost').text($.cookie('ticketCost'));
        $('#cart-tab-route-cities').text($.cookie('destination'));
        $('.cart-tab').show();

      }

      // create table of the schedule's
      writeSchedule.createBigTable();

      // admin area
      $(document).on('click', '#customer-icon,.arrow-mynyair', function() {
        $('#button-my-nyair').trigger('click');
      });

      // change ticket option for buying ticket
      $(document).on(
              'click',
              '.buy-ticket-chk',
              function() {
                var name = $(this).attr('id');
                var checkBox = "input[id='checkbox-" + name + "']";
                var ticketCost = parseInt($('#total-cost-buy-ticket').text());
                var optionCostWithEuro = $('#' + name + '-cost').text();
                var optionCostArray = optionCostWithEuro.toString().split(' ');
                var optionCost = parseInt(optionCostArray[1]);

                if (!$(checkBox).prop('checked')) {
                  $(checkBox).prop('checked', true);
                  $(this).css('background-color', '#073590').css('border',
                          '1px #000 solid').css('color', '#f0c833');

                  ticketCost = ticketCost + optionCost;
                  $('#total-cost-buy-ticket').text(ticketCost);
                  $('#ticket-cost-input').val(ticketCost);

                } else {
                  $(checkBox).prop('checked', false);
                  $(this).css('background-color', '#fafaf6').css('border',
                          '1px #efefef solid').css('color', '#000');

                  ticketCost = ticketCost - optionCost;
                  $('#total-cost-buy-ticket').text(ticketCost);
                  $('#ticket-cost-input').val(ticketCost);
                }

              });

      // scroll schedules
      $(document).on('click', '#button-left-schedule-caorusel', function() {
        $("#schedules").animate({
          "right": "-=121px"
        }, 100);

      });

      $(document).on('click', '#button-right-schedule-caorusel', function() {
        $("#schedules").animate({
          "right": "+=121px"
        }, 100);
      });

      // mouse scroll schedules
      $(document).on('mousewheel', '#schedules', function(e) {
        if (e.deltaY > 0) {
          $("#schedules").animate({
            "right": "-=20px"
          }, 1);

        } else {
          $("#schedules").animate({
            "right": "+=20px"
          }, 1);
        }
        e.preventDefault();
      });

      // customer update command
      $(document).on('click', '#tab-customer-update', function() {
        var msg = $('#update-form').serialize() + '&command=customer-update';
        $.ajax({
          type: 'POST',
          url: 'controller',
          data: msg,
          success: function(json) {
            $('#tab-old-password').val('');
            $('#tab-customer-password').val('');
            msgBox.serverMsg('msgbox', json);
          }
        });

      });

      // customer password update command
      $(document).on(
              'click',
              '#tab-customer-password-update',
              function() {
                var msg = $('#update-form-password').serialize()
                        + '&command=customer-password-update';
                $.ajax({
                  type: 'POST',
                  url: 'controller',
                  data: msg,
                  success: function(json) {
                    $('#tab-old-password').val('');
                    $('#tab-customer-password').val('');
                    msgBox.serverMsg('msgbox', json);
                  }
                });

              });

      $(document).on('click', 'body', function() {
        closeAllHeadersTab.closeWindows();
      });

      // change ticket
      $(document).on(
              'click',
              '.ticket-cost',
              function() {

                $.removeCookie('scheduleId');
                $.removeCookie('business');

                $('.cart-tab').slideDown(16, function() {
                  $('.cart-tab').show();

                });

                if ($(this).attr('id') == 'sch-ticket-cost-business') {

                  $.cookie('business', true);

                }

                $('.ticket-cost').css('background', '');
                $(this).css('background',
                        'linear-gradient(to top, #fff, #d4eafb, #fff)');

                $('.cart-cost').text($(this).text());
                $.cookie('scheduleId', $('#sch-schedule-id').val());
                $.cookie('ticketCost', $(this).text());
                $.cookie('destination', $('#cart-tab-route-cities').text());

              });

      // continue buy ticket
      $(document).on('click', '#continue-buy-ticket', function() {
        $('.ticket-option-list').hide();
        checkLogin.check();

      });

      // close setting tab
      $('#close-button-settings').click(function() {
        $('.settings-tab').hide();
        $('.form-field input').val('');
        $('#buy-ticket-message').hide();
        $('#buy-ticket-message').text('');

      });

      // close my nyair tab
      $('#close-button-mynyair').click(function() {
        $('.mynyair-tab').hide();
        $('#buy-ticket-message').hide();
        $('#buy-ticket-message').text('');

      });

      // register radio button
      $('input[name=signupradiogroup]').change(function() {
        $('#message-login').data('view', 'false');
        $('#message-login').hide();
        $('#password-field').val('');
        $('#login-field').val('');
        $('.alert-message').hide();

        // resize register tab
        if ($(this).val() == 'register') {
          $('#register-form').slideDown(120, function() {
            $('.menu-dropdown').css('height', '554px');
            $('.col-featuers').css('height', '504px');

          });

          $('#register-form').show();
          $('#login-form').hide();

        } else {

          $('#register-form').slideUp(120, function() {
            $('.menu-dropdown').css('height', '338px');
            $('.col-featuers').css('height', '287px');

          });

          $('#register-form').hide();
          $('#login-form').show();

        }

      });

      // register customer
      $('#customer-register').on('click', function() {

        var msg = $('#register-form').serialize() + '&command=customer-create';
        $.ajax({
          type: 'POST',
          url: 'controller',
          data: msg,
          success: function(json) {

            $('#message-register span').text(json.ServerMessage.message);

            if (json.ServerMessage.result == false) {

              $('#message-register').slideDown(100, function() {

                $('.menu-dropdown').css('height', '637px');
                $('.col-featuers').css('height', '587px');
              });
            } else {

              window.location.reload();

            }

          }
        });
      });

      // cancel buying ticket
      $(document).on('click', '#buy-ticket-cancel', function() {
        $('#buy-ticket-tab').hide();

        $.removeCookie('scheduleId');

        $('.cart-tab').hide();
        $('.cart-cost').text(0);

        takeTicketsById.action();

      });

      // buy ticket
      $('#buy-ticket').on('click', function() {

        var msg = $('#buy-ticket-form').serialize() + '&command=ticket-create';
        $.ajax({
          type: 'POST',
          url: 'controller',
          data: msg,
          success: function(json) {

            if (json.ServerMessage != undefined) {

              if (json.ServerMessage.result = true) {

                $('#buy-ticket-tab').hide();

                $.removeCookie('scheduleId');

                $('.cart-tab').hide();
                $('.cart-cost').text(0);

                takeTicketsById.action();

                msgBox.serverMsg('msgbox', json);

              }

            }

          }
        });
      });

      // continue schedule button
      $('#continue-schedule').on('click', function() {

        $.removeCookie('routeId');

        $('.poster-container').hide();
        $('.flight-schedule-selector').hide();
        $('.cart-tab').hide();

        var routeId = $('.arrival-city').data('routeId');

        $.ajax({
          url: 'controller',
          data: {
            command: 'schedule-by-route-id',
            'route-id': routeId
          },
          success: function(json) {

            if (json.ServerMessage == undefined) {

              $('#schedules-cont').show();
              writeSchedule.createTable(json, "#schedules");

            } else {

              msgBox.serverMsg('msgbox', json);

            }

          }
        });
      });

      // back button
      $('#back').on('click', function() {
        $('.flight-schedule-selector').show();

        if ($.cookie("scheduleId") == null) {

          $('.cart-tab').hide();
        }

        $('#schedules-cont').hide();
        $('.poster-container').show();

        closeAllHeadersTab.closeAllTab();

      });

    }
  };

  // admin, click to menu item
  $(document).on('click', '.menu-item', function() {

    $('.console').text('');
    $('.console').hide();

    $('.menu-item').css("background-color", "#FFCC33");
    $(this).css("background-color", "#f4f4f4");
    var name = $(this).attr('name');
    var tableClass = '.' + $(this).attr('name') + '-table';

    $('.editor').hide();
    $(tableClass).show();

    if ($("input[name='" + name + "-city-id']").val() != "") {

      $('#' + name + 'form').show().draggable();
    }

    var command = $(this).attr('name') + '-all';

    takeAll.take(name, command);
    console.log(name, command);

  });

  // admin, hide footer if scroll down
  $(window).scroll(
          function() {
            if ($(window).scrollTop() + $(window).height() > $(document)
                    .height() - 100) {

              $('.footer').hide();

            } else {

              $('.footer').show();

            }
          });

  // admin console - draggable
  $('.console').draggable();

  // admin, load schedules by routeId in schedule edit tab
  $(document).on(
          'change',
          '#route-select-schedule',
          function() {

            $.cookie('routeId', $(this).val());

            var command = 'command=schedule-by-route-id&route-id='
                    + $.cookie('routeId');

            $.ajax({
              url: 'controller',
              data: command,
              success: function(responseText) {

                writeTable.create('schedule', responseText);

              }
            });

          });

  return main;

});