requirejs.config({
  baseUrl: 'js/lib',
  paths: {
    app: '../app',
    md: '../app/module',
    vl: '../app/module/validator',
    ba: '../app/module/button_action',
    html: '../app/module/html_creator',
    msg: '../app/module/msgbox',
    admin: '../app/module/admin',
    'jquery': 'jquery-2.1.4.min',
    'jqui': 'jquery-ui.min',
    'jqcookie': 'jquery.cookie',
    'jqrcode': 'jquery.qrcode.min',
    'domready': 'domready',
    'mwheelIntent': 'mwheelIntent',
    'jqmousewheel': 'jquery.mousewheel',

  }

});

require(['domready', 'app/script'],
        function(domReady, main) {
          domReady(function() {

            main.start();

          });
        });
requirejs(['jquery']);
requirejs(['jqui']);