<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<head>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,700italic,400italic'
	rel='stylesheet' type='text/css'>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script data-main="js/config.js" src="js/lib/require.js"></script>
<title><fmt:message key="index.title" /></title>
	</head>
</fmt:bundle>
