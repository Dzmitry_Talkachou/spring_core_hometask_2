<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="header">
		<div class="logo-nyair" id="back">
			<a href="/nyair"><img src="images/nyair-logo.png"></a>
		</div>

		<div class="my-nyair" id="button-my-nyair" data-click-value="false">
			<c:choose>
				<c:when test="${!empty customerName}">
						${customerName}
					</c:when>
				<c:otherwise>
					<fmt:message key="my.nyair" />
				</c:otherwise>
			</c:choose>
		</div>
		<div class="arrow-mynyair">
			<img src="images/arrow-down.png">
		</div>


		<div class="customer-icon" id="customer-icon"
			<c:if test="${empty customerId}">style="display: none;"</c:if>>
			<img src="images/customer.png">
		</div>

		<div class="flag-cont" data-click-value="false">
			<div class="flag">
				<img
					src="images/${locale}<c:if test="${empty locale}">en_GB</c:if>.png">
			</div>
			<div class="arrow-flag">
				<img src="images/arrow-down.png">
			</div>

		</div>
	</div>
	<div class="cart-tab" style="display: none;">
		<div class="route-info">
			<span class="route-cities" id="cart-tab-route-cities"></span>
		</div>
		<div class="cart-cont">
			<div class="total-cost">
				<span class="total-cost-msg"><fmt:message
						key="message.total.cost" /></span> <span class="cart-cost">0</span>
			</div>
			<input class="core-btn-primary" type="button"
				id="continue-buy-ticket" value="<fmt:message key="continue" />">
		</div>
	</div>
</fmt:bundle>