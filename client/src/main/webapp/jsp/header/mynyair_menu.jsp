<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="mynyair-menu-dropdown" style="display: none;">

		<div class="mynyair-menu-option" id="mynyair-menu-mynyair">
			<img class="mynyair-img" src="images/mynyair.png"> <span><fmt:message
					key="my.nyair" /></span>
		</div>

		<div class="mynyair-menu-option" id="mynyair-menu-settings">
			<img class="mynyair-img" src="images/settings.png"> <span><fmt:message
					key="settings" /></span>
		</div>

		<div class="mynyair-menu-option" id="mynyair-menu-admin"
			style="display: none;">
			<img class="mynyair-img" src="images/role.admin.png"> <span><fmt:message
					key="admin" /></span>
		</div>

		<div class="mynyair-menu-option" id="mynyair-menu-logout">
			<img class="mynyair-img" src="images/logout.png"> <span><fmt:message
					key="logout" /></span>
		</div>


		<img class="mynyair-menu-arrow" src="images/drop-down-arrow.png">
	</div>
</fmt:bundle>