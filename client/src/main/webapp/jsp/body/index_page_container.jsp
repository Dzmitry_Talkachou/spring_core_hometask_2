<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="page-container">
		<!--  jsp:include page="/jsp/body/main_banner.jsp" /-->
	</div>
	<div class="event-container"></div>
	<div class="schedule-container"></div>
	<div class="schedule-by-id-container"></div>
</fmt:bundle>