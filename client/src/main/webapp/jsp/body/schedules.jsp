<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="schedules" id="schedules-cont" style="display: none;">
		<div class="schedule-caorusel">
			<div class="button-left" id="button-left-schedule-caorusel">
				<img src="images/button-left.png">
			</div>
			<div id="schedules"></div>
			<div class="button-right" id="button-right-schedule-caorusel">
				<img src="images/button-right.png">
			</div>
		</div>

		<div class="schedule-concrete" id="schedule-concrete"
			style="display: none;">
			<div class="schedule-con-cont">
				<div class="depart-cont">
					<input type="hidden" id="sch-schedule-id" value=""> <span
						class="route-name" id="sch-route-name"></span> <span
						class="route-depart-city" id="sch-route-depart-city"> </span> <span
						class="depart-time" id="sch-depart-time"></span>
				</div>
				<div class="flight-img">
					<img src="images/flight.png">
				</div>
				<div class="arrival-cont">
					<span class="flight-time" id="sch-flight-time"></span><div class="hour-schedules"><fmt:message
								key="time.hours" /></div><span
						class="route-arrival-city" id="sch-route-arrival-city"></span> <span
						class="arrival-time" id="sch-arrival-time"></span>
				</div>
				<div class="ticket-cont">
					<div class="ticket-type">
						<span class="ticket-cost" id="sch-ticket-cost-standart"></span> <span
							class="plan-type" id="sch-plan-type"><fmt:message
								key="schedules.standart" /></span>
					</div>
					<div class="ticket-type">
						<span class="ticket-cost" id="sch-ticket-cost-business"></span> <span
							class="plan-type" id="sch-plan-type"><fmt:message
								key="schedules.business.plus" /></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</fmt:bundle>