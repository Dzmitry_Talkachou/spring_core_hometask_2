<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="buy-edit" id="edit-buy-ticket" style="display: none;">

		<input class="option-business" type="hidden" name="option-8"
			id="option-business" value="">
		<ctg:additional-option />
		
		<div class="buy-cancel-ticket">
			<input class="core-btn-primary" type="button" name="buy-ticket"
				id="buy-ticket" value="<fmt:message key="buy" />"> <input
				class="core-btn-primary" type="button" name="cancel"
				id="buy-ticket-cancel" value="<fmt:message key="cancel" />">
		</div>

	</div>


</fmt:bundle>