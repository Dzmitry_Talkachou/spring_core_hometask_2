<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="language-menu-dropdown" style="display: none;">
		<div class="flag-dropdown" id="flag-gb">
			<div class="language-select" id="en-language" data-locale="en_GB">
				<img class="flag-img" src="images/en_GB.png">
				<fmt:message key="gb.english" />
			</div>
		</div>

		<div class="flag-dropdown" id="flag-by">
			<div class="language-select" id="ru-language" data-locale="ru_BY">
				<img class="flag-img" src="images/ru_BY.png">
				<fmt:message key="belarus.russian" />
			</div>
		</div>
		<img class="language-menu-arrow" src="images/drop-down-arrow.png">
	</div>
</fmt:bundle>