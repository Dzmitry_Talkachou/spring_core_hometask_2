<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="ticket-message" id="buy-ticket-message"></div>
	<div class="buy-ticket" id="buy-ticket-tab" style="display: none;">
		<form id="buy-ticket-form">
			<input type="hidden" name="schedule-id" id="schedule-id-buy-ticket">
			<div class="schedule-cost-view" id="schedule-id-buy-ticket"></div>
			<div class="route-name">
				<label><fmt:message key="route.number" /></label> <span
					id="route-name-buy-ticket"></span>
			</div>
			<div class="route-cities">
				<label><fmt:message key="direction" /></label> <span
					id="route-cities-buy-ticket"></span>
			</div>
			<div class="departure-date">
				<label><fmt:message key="departure.date" /></label> <span
					id="departure-date-buy-ticket"></span>
			</div>
			<div class="departure-time">
				<label><fmt:message key="departure.time" /></label> <span
					id="departure-time-buy-ticket"></span>
			</div>
			<div class="plane-name">
				<label><fmt:message key="model" /></label> <span
					id="plane-name-buy-ticket"></span>
			</div>
			<div class="plane-name">
				<label><fmt:message key="tail.number" /></label> <span
					id="tail-number-buy-ticket"></span>
			</div>
			<div class="plane-name">
				<label><fmt:message key="message.total.cost" /></label> <fmt:message key="euro" /> <span
					id="total-cost-buy-ticket"></span>
			</div>

			<input type="hidden" name="ticket-cost" id="ticket-cost-input"
				value="">
			<jsp:include page="ticket_option.jsp" />

			<div class="e-ticket-msg" style="display: none;">
				<fmt:message key="eticket" />
			</div>

		</form>
	</div>

</fmt:bundle>