<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="menu-dropdown login" style="display: none;">
		<div class="col-featuers-cont">
			<div class="col-featuers">
				<h3 class="nav-bar-h3">
					<fmt:message key="sing.up.now" />
				</h3>
				<ul class="nav-bar-ul">
					<li><fmt:message key="header.message.one" /></li>
					<li><fmt:message key="header.message.two" /></li>
					<li><fmt:message key="header.message.three" /></li>
				</ul>
			</div>
		</div>
		<div class="col-got-account">
			<div class="col-signup">
				<h3 class="nav-bar-h3">
					<fmt:message key="sing.up.or.login" />
				</h3>
				<input class="core-radio" type="radio" name="signupradiogroup"
					id="signup-radio" value="login" checked="checked"> <label
					for="signupradiogroup"> <fmt:message key="login" />
				</label> <input class="core-radio" type="radio" name="signupradiogroup"
					id="register-raido" value="register"> <label
					for="signupradiogroup"><fmt:message key="create.an.account" /></label>

				<form class="signup" id="login-form">
					<div class="form-field">

						<div class="alert-danger" id="message-login" data-view="false"
							style="display: none;">
							<span></span> <img class="alert-danger-close-button"
								id="alert-danger-close-button" src="images/close.png">
						</div>
					</div>

					<div class="form-field">

						<label for="login"><fmt:message key="email" /> <em
							class="star"><fmt:message key="star" /></em> </label> <input
							type="email" id="login-field" name="login" data-valid="false"
							autocomplete="off"> <em id="login-field-warning"
							style="display: none;"> <fmt:message key="example.email" />
						</em>

					</div>
					<div class="form-field">
						<label for="password"><fmt:message key="password" /> <em
							class="star"><fmt:message key="star" /></em></label> <input
							type="password" class=""  data-valid="false" id="password-field" name="password">

						<em id="password-field-warning" style="display: none;">

							<fmt:message key="example.password" />
						</em>

					</div>

					<div class="form-btn">

						<input type="button" class="core-btn-primary"
							data-error-counter="2" id="login-button"
							value="<fmt:message key="login" />">
					</div>

					<img class="singup-menu-arrow" src="images/drop-down-arrow.png">

				</form>

				<jsp:include page="register_tab.jsp" />

			</div>
		</div>
	</div>
</fmt:bundle>