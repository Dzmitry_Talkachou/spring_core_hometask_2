<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">

	<div class="mynyair-tab" style="display: none;">
		<div class="tab-header">
			<h3>
				<c:choose>
					<c:when test="${!empty customerName}">
						${customerName}
					</c:when>
					<c:otherwise>
						<fmt:message key="my.nyair" />
					</c:otherwise>
				</c:choose>
			</h3>
			<img class="close-button-tab" id="close-button-mynyair"
				src="images/close_button_tab.png">
		</div>
		<jsp:include page="/jsp/tab/tab_menu.jsp" />
		<jsp:include page="/jsp/tab/buy_ticket_tab.jsp" />


		<div class="ticket-option-list" id="ticket-option-list"></div>
		<div class="ticket-list" id="ticket-list"></div>
	</div>
</fmt:bundle>