<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">

	<form class="register" id="register-form" style="display: none;">
		<div class="form-field">

			<div class="alert-danger" id="message-register" data-view="false"
				style="display: none;">
				<span></span><img class="alert-danger-close-button"
					id="alert-danger-close-button-register" src="images/close.png">
			</div>

		</div>
		<div class="form-field">
			<label for="first-name"><fmt:message key="first.name" /> <em class="star"><fmt:message key="star" /></em></label> <input
				id="customer-first-name" name="first-name" type="text"
				data-valid="false" autocomplete="off" value=""> <em
				id="customer-first-name-warning" style="display: none;"> <fmt:message
					key="example.first.name" />

			</em>
		</div>

		<div class="form-field">
			<label for="second-name"><fmt:message key="second.name" /> <em
				class="star"><fmt:message key="star" /></em></label> <input
				id="customer-second-name" name="second-name" type="text"
				data-valid="false" autocomplete="off" value=""> <em
				id="customer-second-name-warning" style="display: none;"> <fmt:message
					key="example.second.name" />

			</em>
		</div>

		<div class="form-field">
			<label for="customer-email"><fmt:message key="email" /> <em
				class="star"><fmt:message key="star" /></em> </label> <input type="email"
				id="customer-email" name="email" data-valid="false"
				autocomplete="off"> <em id="customer-email-warning"
				style="display: none;"> <fmt:message key="example.email" />
			</em>
		</div>
		<div class="form-field">
			<label for="customer-password"><fmt:message key="password" />
				<em class="star"><fmt:message key="star" /></em></label> <input
				type="password" class="" id="customer-password-tab"
				data-valid="false" name="password" autocomplete="off"> <em
				id="customer-password-tab-warning" style="display: none;"> <fmt:message
					key="example.password" />
			</em>
		</div>

		<div class="form-field">
			<label for="payment-method-id"><fmt:message
					key="payment.method" /></label>


			<ctg:payment-method cssClass="payment-method-id-select"
				id="customer-payment-method-id" name="payment-method-id" />



		</div>

		<div class="form-btn">
			<input type="button" class="core-btn-primary" data-error-counter="4"
				id="customer-register" value="<fmt:message key="register" />">
		</div>
		<img class="singup-menu-arrow" src="images/drop-down-arrow.png">
	</form>
</fmt:bundle>