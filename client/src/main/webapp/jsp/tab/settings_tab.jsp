<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="settings-tab" style="display: none;">
		<div class="setting-tab-cont">
			<div class="tab-header">
				<h3>
					<fmt:message key="settings" />
				</h3>
				<img class="close-button-tab" id="close-button-settings"
					src="images/close_button_tab.png">
			</div>
			<jsp:include page="/jsp/tab/tab_menu.jsp" />

			<div class="customer-settings">

				<form class="register" id="update-form">
					<div class="form-field" id="tab-message-register">

						<div class="alert-danger" data-view="false" style="display: none;">
							<span></span> <img class="alert-danger-close-button"
								id="tab-alert-danger-close-button" src="images/close.png">
						</div>

					</div>
					<div class="form-field">
						<input id="tab-customer-id" name="customer-id" type="hidden"
							value=""> <label for="first-name"> <fmt:message
								key="first.name" /> <em class="star"><fmt:message
									key="star" /></em></label> <input id="tab-customer-first-name"
							name="first-name" type="text" data-valid="true"
							autocomplete="off" value=""> <em
							id="tab-customer-first-name-warning" style="display: none;">

							<fmt:message key="example.first.name" />

						</em>


					</div>

					<div class="form-field">
						<label for="second-name"> <fmt:message key="second.name" />
							<em class="star"><fmt:message key="star" /></em></label> <input
							name="second-name" id="tab-customer-second-name" type="text"
							data-valid="true" autocomplete="off" value=""> <em
							id="tab-customer-second-name-warning" style="display: none;">
							<fmt:message key="example.second.name" />

						</em>

					</div>

					<input type="hidden" id="customer-email" name="email" value="">


					<div class="form-field">
						<label for="payment-method-id"> <fmt:message
								key="payment.method" /></label>

						<ctg:payment-method cssClass=""
							id="tab-customer-payment-method-id" name="payment-method-id" />


					</div>

					<div class="form-btn">
						<input type="button" class="core-btn-primary"
							id="tab-customer-update"
							value="<fmt:message key="button.update" />">
					</div>
				</form>

				<form class="register" id="update-form-password">

					<input id="tab-customer-id-pass" name="customer-id" type="hidden"
						value="">
					<div class="form-field">
						<label for="old-password"><fmt:message key="old.password" />
							<em class="star"><fmt:message key="star" /></em></label> <input
							type="password" class="" data-valid="false" id="tab-old-password"
							name="old-password"> <em id="tab-old-password-warning"
							style="display: none;"> <fmt:message key="example.password" />
						</em>


					</div>

					<div class="form-field">
						<label for="customer-password"> <fmt:message
								key="new.password" /></label> <em class="star"><fmt:message
								key="star" /></em><input type="password" class=""
							id="tab-customer-password" data-valid="false" name="password">

						<em id="tab-customer-password-warning" style="display: none;">

							<fmt:message key="example.password" />
						</em>

					</div>

					<div class="form-btn">
						<input type="button" class="core-btn-primary"
							data-error-counter="2" id="tab-customer-password-update"
							value="<fmt:message key="button.update" />">
					</div>
				</form>
			</div>

		</div>
	</div>
</fmt:bundle>