<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="pagecontent.pagecontent" prefix="page.">
	<div class="footer">
		<div class="copyright">
			<fmt:message key="all.rights.reserved" />
			.
		</div>
		<div class="contact-us"></div>
	</div>
	<div id="locale-info" data-locale="${locale}"></div>
</fmt:bundle>