<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}" />
<!DOCTYPE html>
<html>
<!-- head -->
<jsp:include page="/jsp/head/head.jsp" />

<body>
	<!-- message -->
	<jsp:include page="/jsp/message/message.jsp" />

	<!-- header -->
	<jsp:include page="/jsp/header/header.jsp" />
	<jsp:include page="/jsp/header/mynyair_menu.jsp" />

	<!-- tabs -->
	<jsp:include page="/jsp/tab/login_tab.jsp" />
	<jsp:include page="/jsp/tab/register_tab.jsp" />
	<jsp:include page="/jsp/tab/mynyair_tab.jsp" />
	<jsp:include page="/jsp/tab/languauge_select_tab.jsp" />
	<jsp:include page="/jsp/tab/buy_ticket_tab.jsp" />
	<jsp:include page="/jsp/tab/settings_tab.jsp" />

	<!-- body -->
	<jsp:include page="/jsp/body/403_error.jsp" />

	<!-- footer -->
	<jsp:include page="/jsp/footer/footer.jsp" />

</body>
</html>