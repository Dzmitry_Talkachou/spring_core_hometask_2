<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<fmt:setLocale value="${locale}" />
<c:if
	test="${requestScope['javax.servlet.forward.request_uri'] !='/nyair/404'}">
	<c:redirect url="/404" />
</c:if>
<!DOCTYPE html>
<html>
<!-- head -->
<jsp:include page="/jsp/head/head.jsp" />

<body>
	<!-- message -->
	<jsp:include page="/jsp/message/message.jsp" />

	<!-- header -->
	<jsp:include page="/jsp/header/header.jsp" />
	<jsp:include page="/jsp/header/mynyair_menu.jsp" />

	<!-- tabs -->
	<jsp:include page="/jsp/tab/login_tab.jsp" />
	<jsp:include page="/jsp/tab/register_tab.jsp" />
	<jsp:include page="/jsp/tab/mynyair_tab.jsp" />
	<jsp:include page="/jsp/tab/languauge_select_tab.jsp" />
	<jsp:include page="/jsp/tab/buy_ticket_tab.jsp" />
	<jsp:include page="/jsp/tab/settings_tab.jsp" />

	<!-- body -->
	<jsp:include page="/jsp/body/404_error.jsp" />

	<!-- footer -->
	<jsp:include page="/jsp/footer/footer.jsp" />

</body>

</html>